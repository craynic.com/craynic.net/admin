// noinspection JSUnresolvedFunction,JSUnresolvedVariable

db = new Mongo().getDB("db");

db.domains.insertMany(
    [
        {
            _id: ObjectId("000000000000000000000001"),
            id: UUID("8d5ecf47-59fe-4f6c-a376-01456288dc77"),
            domain: "example.com",
            description: "",
            isActive: true,
            isLoginEnabled: true,
            isDeliveryEnabled: true,
            dkim: {
                isEnabled: true,
                key: "123123123",
                selector: "craynic"
            },
            aliases: [
                {
                    domain: "example.net",
                    isActive: true,
                    isDeliveryEnabled: true,
                },
            ],
            mailboxes: [
                {
                    id: UUID("daab7455-b6b5-412f-b62a-62f8eb3e2959"),
                    mailbox: "ales",
                    description: "",
                    // "12345"
                    password: "$2y$10$VoFfA.4EzcyuSpiwl.52uuPuQPPXHG5lAkR8UWm6LdJSGUEhUqreq",
                    isActive: true,
                    isLoginEnabled: true,
                    isDeliveryEnabled: true,
                    isCatchAll: false,
                    extraSenders: [
                        {
                            sender: "boss@example.com",
                            isActive: true,
                        },
                    ],
                    bcc: {
                        bcc: "backup@example.com",
                        isActive: true,
                    },
                    appPasswords: [
                        {
                            // "abcde"
                            password: "$2y$10$y8odRNy3Tpz/X9aUbGJ9q.bnuemkym672vL.jXeJG6lFAVlgBjC/S",
                            description: "Mail client password",
                        }
                    ],
                },
                {
                    id: UUID("352b0289-c827-493b-a75e-cb2d4c382c37"),
                    mailbox: "bara",
                    description: "",
                    // "54321"
                    password: "$2y$10$rog2IsvFFsy1VoftlcZj2e74PebtJP0JtH99Gxo7DSSp7HDVgfjwK",
                    isActive: true,
                    isLoginEnabled: true,
                    isDeliveryEnabled: true,
                    isCatchAll: false,
                    bcc: {
                        bcc: "backup@example.com",
                        isActive: true,
                    },
                    appPasswords: [
                        {
                            // "edcba"
                            password: "$2y$10$rqHYVzf6qyc6BFG6pCUWguZDJmYIhiDY84aqHSPLArOwxmXoOJ7.q",
                            description: "Mail client password",
                        }
                    ],
                },
            ],
            redirects: [
                {
                    id: UUID("95b64c19-d06e-4a4e-84cf-4418bae9b40f"),
                    mailbox: "info",
                    description: "",
                    targets: [
                        {
                            target: "adam@example.com",
                            isActive: true,
                        },
                        {
                            target: "betty@example.com",
                            isActive: true
                        }
                    ],
                    isActive: true,
                }
            ],
        },
    ]
);
