// noinspection JSUnresolvedFunction,JSUnresolvedVariable

db = new Mongo().getDB("db");

db.quota.insertMany(
    [
        {
            id: UUID("3b20104e-eb61-4313-aa7f-8eddffbcbaa7"),
            domains: [
                {
                    domain: "example.com",
                    usage: {
                        current: NumberInt("0"),
                        max: NumberInt("10485760"),
                        pctWarning: {
                            levels: [NumberInt("85"), NumberInt("95"), NumberInt("99")],
                            lastWarning: {
                                level: NumberInt("85"),
                                notificationSent: false
                            }
                        }
                    },
                    mailboxes: [
                        {
                            mailbox: "ales",
                            usage: {
                                current: NumberInt("0"),
                                max: NumberInt("1048576"),
                                pctWarning: {
                                    levels: [NumberInt("85"), NumberInt("95"), NumberInt("99")],
                                    lastWarning: {
                                        level: NumberInt("85"),
                                        notificationSent: false
                                    }
                                }
                            },
                        }
                    ]
                }
            ],
            usage: {
                current: NumberInt("0"),
                max: NumberInt("104857600"),
                pctWarning: {
                    levels: [NumberInt("85"), NumberInt("95"), NumberInt("99")],
                    lastWarning: {
                        level: NumberInt("85"),
                        notificationSent: false
                    }
                }
            }
        }
    ]
);
