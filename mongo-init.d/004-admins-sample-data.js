// noinspection JSUnresolvedFunction,JSUnresolvedVariable

db = new Mongo().getDB("db");

db.admins.insertMany(
    [
        {
            id: UUID("67491b73-8f8c-467b-97c2-85dc0162446c"),
            version: 1,
            username: "admin",
            email: "ales@example.com",
            // "12345"
            password: "$2y$10$VoFfA.4EzcyuSpiwl.52uuPuQPPXHG5lAkR8UWm6LdJSGUEhUqreq",
            isSuperAdmin: true
        },
        {
            id: UUID("d7b00e29-0fa1-4e3a-ada7-7be359c3b727"),
            version: 1,
            username: "localadmin",
            email: "bara@example.com",
            domains: [
                ObjectId("000000000000000000000001"),
            ],
        }
    ]
);
