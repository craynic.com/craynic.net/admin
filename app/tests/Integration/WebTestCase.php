<?php

namespace Tests\App\Integration;

use App\Kernel;
use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use Doctrine\ODM\MongoDB\DocumentManager;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\Security\Core\User\UserInterface;

abstract class WebTestCase extends \Symfony\Bundle\FrameworkBundle\Test\WebTestCase
{
    protected KernelBrowser $browserClient;
    protected Router $router;
    private MongoDBTestingHelper $mongoDbTestingHelper;
    private MySQLTestingHelper $mySQLTestingHelper;

    public function setUp(): void
    {
        parent::setUp();

        $this->browserClient = self::createClient();

        /** @var Router $router */
        $router = self::getContainer()->get('router');
        $this->router = $router;

        /** @var ManagerRegistry $mongoDBRegistry */
        $mongoDBRegistry = self::getContainer()->get('doctrine_mongodb');
        /** @var DocumentManager $dm */
        $dm = $mongoDBRegistry->getManager();

        $this->mongoDbTestingHelper = new MongoDBTestingHelper(self::$kernel, $dm);
        $this->mySQLTestingHelper = new MySQLTestingHelper();
    }

    protected function prepareMongoDB(string $fixturesDir = null): void
    {
        $this->mongoDbTestingHelper->setup($fixturesDir);
    }

    protected function prepareMySQL(string $fixturesDir = null): void
    {
        $this->mySQLTestingHelper->setup($fixturesDir);
    }

    public function tearDown(): void
    {
        $this->mongoDbTestingHelper->teardown();
        $this->mySQLTestingHelper->teardown();

        parent::tearDown();
    }

    protected static function getKernelClass(): string
    {
        return Kernel::class;
    }

    protected function getBasicAuthHeader(string $user, string $password): string
    {
        return sprintf('Basic %s', base64_encode(sprintf('%s:%s', $user, $password)));
    }

    protected function assertJsonContainsSubset(string|array $subset, string|array $json, string $message = ''): void
    {
        $this->assertArrayContainsSubset(
            is_string($subset) ? json_decode($subset) : $subset,
            is_string($json) ? json_decode($json, true) : $json,
            $message
        );
    }

    protected function assertArrayContainsSubset(array $subsetArray, array $array, string $message = ''): void
    {
        $this->assertSame(
            array_intersect_key($array, $subsetArray),
            $subsetArray,
            $message
        );
    }

    protected function loginMockUser(
        string $username = null,
        array $roles = null,
        string $firewallContext = 'main'
    ): void {
        $mockUser = $this->getMockBuilder(UserInterface::class)
            ->getMock();

        if (null !== $roles) {
            $mockUser
                ->expects($this->any())
                ->method('getRoles')
                ->willReturn($roles);
        }

        if (null !== $username) {
            $mockUser
                ->expects($this->any())
                ->method('getUserIdentifier')
                ->willReturn($username);
        }

        /** @var UserInterface $mockUser */
        $this->browserClient->loginUser($mockUser, $firewallContext);
    }
}
