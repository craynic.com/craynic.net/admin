<?php

declare(strict_types=1);

namespace Tests\App\Integration\Controller;

use App\EventSubscriber\AuthorizationRequestResolveSubscriber;
use App\ValueObject\OAuth2Scope;
use DateTimeImmutable;
use Doctrine\ORM\EntityManager;
use Generator;
use Lcobucci\JWT\Encoding\JoseEncoder;
use Lcobucci\JWT\Token\Parser;
use League\Bundle\OAuth2ServerBundle\Model\AccessToken;
use League\Bundle\OAuth2ServerBundle\Model\Client;
use League\Bundle\OAuth2ServerBundle\Model\RefreshToken;
use League\Bundle\OAuth2ServerBundle\ValueObject\Grant;
use League\Bundle\OAuth2ServerBundle\ValueObject\RedirectUri;
use League\Bundle\OAuth2ServerBundle\ValueObject\Scope;
use League\Bundle\OAuth2ServerBundle\OAuth2Grants;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Tests\App\Integration\WebTestCase;

final class ApiOAuth2ControllerTest extends WebTestCase
{
    private const VALID_USERNAME = 'active_user@example.com';
    private const VALID_USER_PASSWORD = '12345';
    private const VALID_APP_PASSWORD1 = 'abcde';
    private const VALID_APP_PASSWORD2 = 'fghij';
    private const INVALID_USERNAME = 'non_existent_user@example.com';
    private const INVALID_APP_PASSWORD = 'abcdefghij';
    private const ANOTHER_VALID_USERNAME = 'another_active_user@example.com';
    private const LOGIN_DISABLED_USERNAME = 'login_disabled_user@example.com';
    private const INACTIVE_USERNAME = 'inactive_user@example.com';
    private const VALID_USERNAME_ALIAS = 'active_user@example.net';

    private EntityManager $entityManager;

    public function setUp(): void
    {
        parent::setUp();

        /** @noinspection PhpFieldAssignmentTypeMismatchInspection */
        $this->entityManager = self::getContainer()->get('doctrine.orm.entity_manager');
    }

    public function testTokenActionReturnBadRequestWithMissingParameters(): void
    {
        $this->prepareMySQL();
        $this->prepareMongoDB(__DIR__ . '/MongoDBFixture1');

        $this->browserClient->request(Request::METHOD_POST, $this->router->generate('api-oauth2-token'));

        $this->assertErrorJsonResponse('unsupported_grant_type', 400);
    }

    /**
     * @dataProvider passwordGrantIssuesTokenUsingCredentialsWithApplicationPasswordDataProvider
     */
    public function testPasswordGrantIssuesTokenUsingCredentialsWithApplicationPassword(string $password): void
    {
        // testing data
        $clientId = '1234567890';
        $clientSecret = '123456789012345678901234567890';
        $redirectUri = 'https://redirect/uri';
        $clientScopes = OAuth2Scope::EMAIL->value;
        $requestScopes = $clientScopes;
        $clientGrants = [OAuth2Grants::PASSWORD];
        $username = self::VALID_USERNAME;

        $this->prepareMySQL();
        $this->prepareMongoDB(__DIR__ . '/MongoDBFixture1');

        $this->createOAuth2Client($clientId, $clientSecret, $clientScopes, $redirectUri, $clientGrants);

        $this->browserClient->request(
            Request::METHOD_POST,
            $this->router->generate('api-oauth2-token'),
            [
                'grant_type' => OAuth2Grants::PASSWORD,
                'client_id' => $clientId,
                'client_secret' => $clientSecret,
                'scope' => $requestScopes,
                'username' => $username,
                'password' => $password,
            ]
        );

        $this->assertAccessTokenResponse($clientId, $username, $requestScopes, $this->getName());
    }

    public function passwordGrantIssuesTokenUsingCredentialsWithApplicationPasswordDataProvider(): Generator
    {
        yield 'app password 1' => ['password' => self::VALID_APP_PASSWORD1];
        yield 'app password 2' => ['password' => self::VALID_APP_PASSWORD2];
    }

    /**
     * @dataProvider passwordGrantThrowsExceptionWithWrongCredentialsDataProvider
     */
    public function testPasswordGrantThrowsExceptionWithWrongCredentials(string $username, string $password): void
    {
        $clientId = '1234567890';
        $clientSecret = '123456789012345678901234567890';
        $redirectUri = 'https://redirect/uri';
        $clientScopes = OAuth2Scope::EMAIL->value;
        $requestScopes = $clientScopes;
        $clientGrants = [OAuth2Grants::PASSWORD];

        $this->prepareMySQL();
        $this->prepareMongoDB(__DIR__ . '/MongoDBFixture1');

        $this->createOAuth2Client($clientId, $clientSecret, $clientScopes, $redirectUri, $clientGrants);

        $this->browserClient->request(
            Request::METHOD_POST,
            $this->router->generate('api-oauth2-token'),
            [
                'grant_type' => OAuth2Grants::PASSWORD,
                'client_id' => $clientId,
                'client_secret' => $clientSecret,
                'scope' => $requestScopes,
                'username' => $username,
                'password' => $password,
            ]
        );

        $this->assertErrorJsonResponse('invalid_grant', 400, $this->getName());
    }

    public function passwordGrantThrowsExceptionWithWrongCredentialsDataProvider(): Generator
    {
        yield 'user password instead of application password' => [
            'user' => self::VALID_USERNAME,
            'password' => self::VALID_USER_PASSWORD,
        ];

        yield 'wrong password' => [
            'user' => self::VALID_USERNAME,
            'password' => self::INVALID_APP_PASSWORD,
        ];

        yield 'wrong username' => [
            'user' => self::INVALID_USERNAME,
            'password' => self::VALID_APP_PASSWORD1,
        ];

        yield 'login-disabled username' => [
            'user' => self::LOGIN_DISABLED_USERNAME,
            'password' => self::VALID_APP_PASSWORD1,
        ];

        yield 'inactive username' => [
            'user' => self::INACTIVE_USERNAME,
            'password' => self::VALID_APP_PASSWORD1,
        ];

        yield 'alias username is not working ATM' => [
            'user' => self::VALID_USERNAME_ALIAS,
            'password' => self::VALID_APP_PASSWORD1,
        ];
    }

    /**
     * @dataProvider passwordGrantThrowsExceptionWithWrongClientCredentialsDataProvider
     */
    public function testPasswordGrantThrowsExceptionWithWrongClientCredentials(
        ?string $requestClientId,
        ?string $requestClientSecret,
        ?array $clientGrants,
    ): void {
        $clientId = '1234567890';
        $clientSecret = '123456789012345678901234567890';
        $redirectUri = 'https://redirect/uri';
        $clientScopes = OAuth2Scope::EMAIL->value;
        $requestScopes = $clientScopes;
        $username = self::VALID_USERNAME;
        $password = self::VALID_APP_PASSWORD1;

        $this->prepareMySQL();
        $this->prepareMongoDB(__DIR__ . '/MongoDBFixture1');

        $this->createOAuth2Client(
            $clientId,
            $clientSecret,
            $clientScopes,
            $redirectUri,
            $clientGrants ?? [OAuth2Grants::PASSWORD]
        );

        $this->browserClient->request(
            Request::METHOD_POST,
            $this->router->generate('api-oauth2-token'),
            [
                'grant_type' => OAuth2Grants::PASSWORD,
                'client_id' => $requestClientId ?? $clientId,
                'client_secret' => $requestClientSecret ?? $clientSecret,
                'scope' => $requestScopes,
                'username' => $username,
                'password' => $password,
            ]
        );

        $this->assertErrorJsonResponse('invalid_client', 401, $this->getName());
    }

    public function passwordGrantThrowsExceptionWithWrongClientCredentialsDataProvider(): Generator
    {
        yield 'wrong client_id' => [
            'client_id' => '9876543210',
            'client_secret' => null,
            'grants' => null,
        ];

        yield 'wrong client_secret' => [
            'client_id' => null,
            'client_secret' => '999999999999999999999999999999',
            'grants' => null,
        ];

        yield 'wrong client_id and client_secret' => [
            'client_id' => '9876543210',
            'client_secret' => '999999999999999999999999999999',
            'grants' => null,
        ];

        yield 'wrong grants' => [
            'client_id' => null,
            'client_secret' => null,
            'grants' => [OAuth2Grants::AUTHORIZATION_CODE],
        ];
    }

    public function testPasswordGrantThrowsExceptionWithWrongScope(): void
    {
        $clientId = '1234567890';
        $clientSecret = '123456789012345678901234567890';
        $redirectUri = 'https://redirect/uri';
        $clientScopes = OAuth2Scope::EMAIL->value;
        $requestScopes = 'non-existent-scope';
        $clientGrants = [OAuth2Grants::PASSWORD];
        $username = self::VALID_USERNAME;
        $password = self::VALID_APP_PASSWORD1;

        $this->prepareMySQL();
        $this->prepareMongoDB(__DIR__ . '/MongoDBFixture1');

        $this->createOAuth2Client($clientId, $clientSecret, $clientScopes, $redirectUri, $clientGrants);

        $this->browserClient->request(
            Request::METHOD_POST,
            $this->router->generate('api-oauth2-token'),
            [
                'grant_type' => OAuth2Grants::PASSWORD,
                'client_id' => $clientId,
                'client_secret' => $clientSecret,
                'scope' => $requestScopes,
                'username' => $username,
                'password' => $password,
            ]
        );

        $this->assertErrorJsonResponse('invalid_scope', 400);
    }

    public function testAuthorizationCodeGrantRedirectsUsingUserCredentialsInStep1(): void
    {
        // testing data
        $clientId = '1234567890';
        $clientSecret = '123456789012345678901234567890';
        $redirectUri = 'https://redirect/uri';
        $clientScopes = OAuth2Scope::EMAIL->value;
        $requestScopes = $clientScopes;
        $clientGrants = [OAuth2Grants::AUTHORIZATION_CODE];
        $state = 'qwertz';

        $this->prepareMySQL();
        $this->prepareMongoDB(__DIR__ . '/MongoDBFixture1');

        $this->createOAuth2Client($clientId, $clientSecret, $clientScopes, $redirectUri, $clientGrants);

        $authorizeRequestParameters = [
            'response_type' => 'code',
            'client_id' => $clientId,
            'redirect_uri' => $redirectUri,
            'scope' => $requestScopes,
            'state' => $state,
        ];

        $this->browserClient->request(
            Request::METHOD_GET,
            $this->router->generate('api-oauth2-authorize'),
            $authorizeRequestParameters
        );

        $this->assertResponseRedirects($this->router->generate('oauth2-consent-form', $authorizeRequestParameters));
        $this->assertResponseTypeJson();
    }

    public function testAuthorizationCodeGrantWorksWhenUserApproves(): void
    {
        // testing data
        $clientId = '1234567890';
        $clientSecret = '123456789012345678901234567890';
        $redirectUri = 'https://redirect/uri';
        $clientScopes = OAuth2Scope::EMAIL->value;
        $requestScopes = $clientScopes;
        $clientGrants = [OAuth2Grants::AUTHORIZATION_CODE];
        $state = 'qwertz';
        $username = self::VALID_USERNAME;

        $this->prepareMySQL();
        $this->prepareMongoDB(__DIR__ . '/MongoDBFixture1');

        $this->createOAuth2Client($clientId, $clientSecret, $clientScopes, $redirectUri, $clientGrants);

        $authorizeRequestParameters = [
            'response_type' => 'code',
            'client_id' => $clientId,
            'redirect_uri' => $redirectUri,
            'scope' => $requestScopes,
            'state' => $state,
        ];

        /** @var EventDispatcher $eventDispatcher */
        $eventDispatcher = $this->browserClient->getContainer()->get('event_dispatcher');
        $eventDispatcher->addListener(
            KernelEvents::REQUEST,
            function (RequestEvent $event) {
                $event->getRequest()->getSession()->set(
                    AuthorizationRequestResolveSubscriber::SESSION_AUTHORIZATION_RESULT,
                    true
                );
            }
        );

        $this->loginMockUser($username, ['ROLE_OAUTH2_EMAIL'], 'api');

        $this->browserClient->request(
            Request::METHOD_GET,
            $this->router->generate('api-oauth2-authorize'),
            $authorizeRequestParameters
        );
        $response = $this->browserClient->getResponse();

        $this->assertResponseRedirects();
        $this->assertResponseTypeJson();

        // assert the location was returned
        $this->assertResponseHasHeader('Location');
        $location = $response->headers->get('Location');

        $this->assertStringStartsWith($redirectUri, $location);
        $parsedLocationParameters = [];
        parse_str(parse_url($location, PHP_URL_QUERY), $parsedLocationParameters);

        $this->assertArrayHasKey('code', $parsedLocationParameters);
        $this->assertArrayHasKey('state', $parsedLocationParameters);
        $this->assertSame($state, $parsedLocationParameters['state']);

        $authorizationCode = $parsedLocationParameters['code'];

        // part 2 - request token
        $this->browserClient->request(
            Request::METHOD_POST,
            $this->router->generate('api-oauth2-token'),
            [
                'grant_type' => OAuth2Grants::AUTHORIZATION_CODE,
                'client_id' => $clientId,
                'client_secret' => $clientSecret,
                'redirect_uri' => $redirectUri,
                'code' => $authorizationCode,
            ]
        );

        $this->assertAccessTokenResponse($clientId, $username, $requestScopes);
    }

    public function testAuthorizationCodeGrantRedirectsBackWhenUserRejects(): void
    {
        // testing data
        $clientId = '1234567890';
        $clientSecret = '123456789012345678901234567890';
        $redirectUri = 'https://redirect/uri';
        $clientScopes = OAuth2Scope::EMAIL->value;
        $requestScopes = $clientScopes;
        $clientGrants = [OAuth2Grants::AUTHORIZATION_CODE];
        $state = 'qwertz';
        $username = self::VALID_USERNAME;

        $this->prepareMySQL();
        $this->prepareMongoDB(__DIR__ . '/MongoDBFixture1');

        $this->createOAuth2Client($clientId, $clientSecret, $clientScopes, $redirectUri, $clientGrants);

        $authorizeRequestParameters = [
            'response_type' => 'code',
            'client_id' => $clientId,
            'redirect_uri' => $redirectUri,
            'scope' => $requestScopes,
            'state' => $state,
        ];

        /** @var EventDispatcher $eventDispatcher */
        $eventDispatcher = $this->browserClient->getContainer()->get('event_dispatcher');
        $eventDispatcher->addListener(
            KernelEvents::REQUEST,
            function (RequestEvent $event) {
                $event->getRequest()->getSession()->set(
                    AuthorizationRequestResolveSubscriber::SESSION_AUTHORIZATION_RESULT,
                    false
                );
            }
        );

        $this->loginMockUser($username, ['ROLE_OAUTH2_EMAIL'], 'api');

        $this->browserClient->request(
            Request::METHOD_GET,
            $this->router->generate('api-oauth2-authorize'),
            $authorizeRequestParameters
        );
        $response = $this->browserClient->getResponse();

        $this->assertResponseRedirects();
        $this->assertResponseTypeJson();

        // assert the location was returned
        $this->assertResponseHasHeader('Location');
        $location = $response->headers->get('location');

        $this->assertStringStartsWith($redirectUri, $location);
        $parsedLocationParameters = [];
        parse_str(parse_url($location, PHP_URL_QUERY), $parsedLocationParameters);

        $this->assertArrayHasKey('error', $parsedLocationParameters);
        $this->assertSame('access_denied', $parsedLocationParameters['error']);
        $this->assertArrayHasKey('state', $parsedLocationParameters);
        $this->assertSame($state, $parsedLocationParameters['state']);
    }

    /**
     * @dataProvider authorizationCodeGrantThrowsExceptionWithInvalidClientCredentialsDataProvider
     */
    public function testAuthorizationCodeGrantThrowsExceptionWithInvalidClientCredentials(
        ?string $requestedClientId,
        ?string $requestedRedirectUri,
    ): void {
        // testing data
        $clientId = '1234567890';
        $clientSecret = '123456789012345678901234567890';
        $redirectUri = 'https://redirect/uri';
        $clientScopes = OAuth2Scope::EMAIL->value;
        $requestScopes = $clientScopes;
        $clientGrants = [OAuth2Grants::AUTHORIZATION_CODE];
        $state = 'qwertz';

        $this->prepareMySQL();
        $this->prepareMongoDB(__DIR__ . '/MongoDBFixture1');

        $this->createOAuth2Client($clientId, $clientSecret, $clientScopes, $redirectUri, $clientGrants);

        $this->browserClient->request(
            Request::METHOD_GET,
            $this->router->generate('api-oauth2-authorize'),
            [
                'response_type' => 'code',
                'client_id' => $requestedClientId ?? $clientId,
                'redirect_uri' => $requestedRedirectUri ?? $redirectUri,
                'scope' => $requestScopes,
                'state' => $state,
            ]
        );

        $this->assertErrorJsonResponse('invalid_client', 401, $this->getName());
    }

    public function authorizationCodeGrantThrowsExceptionWithInvalidClientCredentialsDataProvider(): Generator
    {
        yield 'wrong client_id' => [
            'client_id' => '9876543210',
            'redirect_uri' => null,
        ];

        yield 'wrong redirect_uri' => [
            'client_id' => null,
            'redirect_uri' => 'https://wrong-redirect/wrong-uri',
        ];
    }

    public function testAuthorizationCodeGrantRedirectsWithErrorOnWrongScope(): void
    {
        // testing data
        $clientId = '1234567890';
        $clientSecret = '123456789012345678901234567890';
        $redirectUri = 'https://redirect/uri';
        $clientScopes = OAuth2Scope::EMAIL->value;
        $requestScopes = sprintf('%s non-existent-scope', OAuth2Scope::EMAIL->value);
        $clientGrants = [OAuth2Grants::AUTHORIZATION_CODE];
        $state = 'qwertz';

        $this->prepareMySQL();
        $this->prepareMongoDB(__DIR__ . '/MongoDBFixture1');

        $this->createOAuth2Client($clientId, $clientSecret, $clientScopes, $redirectUri, $clientGrants);

        $this->browserClient->request(
            Request::METHOD_GET,
            $this->router->generate('api-oauth2-authorize'),
            [
                'response_type' => 'code',
                'client_id' => $clientId,
                'redirect_uri' => $redirectUri,
                'scope' => $requestScopes,
                'state' => $state,
            ]
        );
        $response = $this->browserClient->getResponse();

        $this->assertResponseRedirects();
        $this->assertResponseTypeJson();
        $this->assertResponseHasHeader('Location');

        $location = $response->headers->get('location');
        $this->assertStringStartsWith($redirectUri, $location);
        $this->assertStringContainsString('error=invalid_scope', $location);
    }

    public function testIntrospectionEndpointRespondsForbiddenWithoutAuthentication(): void
    {
        $this->browserClient->request(
            Request::METHOD_GET,
            $this->router->generate('api-oauth2-introspection'),
        );

        $this->assertResponseStatusCodeSame(401);
    }

    public function testIntrospectionEndpointRespondsForbiddenWithWrongAuthentication(): void
    {
        $this->browserClient->request(
            Request::METHOD_GET,
            $this->router->generate('api-oauth2-introspection'),
            server: [
                'HTTP_AUTHORIZATION' => 'Bearer invalid-bearer-token',
            ]
        );

        $this->assertResponseStatusCodeSame(401);
    }

    public function testIntrospectionEndpointRespondsForbiddenWithWrongTypeOfUser(): void
    {
        $this->loginMockUser(roles: ['ROLE_OAUTH2_WRONGROLE'], firewallContext: 'api');

        $this->browserClient->request(
            Request::METHOD_GET,
            $this->router->generate('api-oauth2-introspection')
        );

        $this->assertResponseStatusCodeSame(403);
    }

    public function testIntrospectionEndpointRespondsServerErrorForWrongUserWithCorrectRole(): void
    {
        // mock user is not OAuth2UserInterface -> this will fail in the introspectionAction method
        $this->loginMockUser(self::VALID_USERNAME, roles: ['ROLE_OAUTH2_EMAIL'], firewallContext: 'api');

        $this->browserClient->request(
            Request::METHOD_GET,
            $this->router->generate('api-oauth2-introspection')
        );

        // this is not something that should happen => hence server error 5xx
        $this->assertResponseStatusCodeSame(500);
    }

    public function testIntrospectionEndpointReturnsDataForAuthenticatedUser(): void
    {
        // testing data
        $clientId = '1234567890';
        $clientSecret = '123456789012345678901234567890';
        $redirectUri = 'https://redirect/uri';
        $clientScopes = OAuth2Scope::EMAIL->value;
        $clientGrants = [OAuth2Grants::PASSWORD];
        $username = self::VALID_USERNAME;
        $password = self::VALID_APP_PASSWORD1;

        $this->prepareMySQL();
        $this->prepareMongoDB(__DIR__ . '/MongoDBFixture1');

        $client = $this->createOAuth2Client($clientId, $clientSecret, $clientScopes, $redirectUri, $clientGrants);

        $accessToken = $this->requestBearerToken($client, $username, $password);

        $this->browserClient->request(
            Request::METHOD_GET,
            $this->router->generate('api-oauth2-introspection'),
            server: [
                'HTTP_AUTHORIZATION' => sprintf('Bearer %s', $accessToken),
            ]
        );
        $response = $this->browserClient->getResponse();

        $this->assertResponseTypeJson(200);

        $this->assertJsonContainsSubset([
            'username' => $username,
            'active' => true,
            'scope' => $clientScopes,
            'client_id' => $clientId,
        ], $response->getContent());
    }

    public function testIntrospectionEndpointReturns401WhenAccessTokenDoesNotExistInDatabaseAnymore(): void
    {
        // testing data
        $clientId = '1234567890';
        $clientSecret = '123456789012345678901234567890';
        $redirectUri = 'https://redirect/uri';
        $clientScopes = OAuth2Scope::EMAIL->value;
        $clientGrants = [OAuth2Grants::PASSWORD];
        $username = self::VALID_USERNAME;
        $password = self::VALID_APP_PASSWORD1;

        $this->prepareMySQL();
        $this->prepareMongoDB(__DIR__ . '/MongoDBFixture1');

        $client = $this->createOAuth2Client($clientId, $clientSecret, $clientScopes, $redirectUri, $clientGrants);

        $accessToken = $this->requestBearerToken($client, $username, $password);

        $accessTokenRepo = $this->entityManager->getRepository(AccessToken::class);
        $accessTokenModels = $accessTokenRepo->findAll();
        $this->assertCount(1, $accessTokenModels);
        $accessTokenModel = $accessTokenModels[0];
        $this->entityManager->remove($accessTokenModel);
        $this->entityManager->flush();

        $this->browserClient->request(
            Request::METHOD_GET,
            $this->router->generate('api-oauth2-introspection'),
            server: [
                'HTTP_AUTHORIZATION' => sprintf('Bearer %s', $accessToken),
            ]
        );

        $this->assertResponseStatusCodeSame(401);
    }

    public function testUserinfoEndpointReturnsUserInfoWithAccessTokenInQuery(): void
    {
        // testing data
        $clientId = '1234567890';
        $clientSecret = '123456789012345678901234567890';
        $redirectUri = 'https://redirect/uri';
        $clientScopes = OAuth2Scope::EMAIL->value;
        $clientGrants = [OAuth2Grants::PASSWORD];
        $username = self::VALID_USERNAME;
        $password = self::VALID_APP_PASSWORD1;

        $this->prepareMySQL();
        $this->prepareMongoDB(__DIR__ . '/MongoDBFixture1');

        $client = $this->createOAuth2Client($clientId, $clientSecret, $clientScopes, $redirectUri, $clientGrants);

        $accessToken = $this->requestBearerToken($client, $username, $password);

        $this->browserClient->request(
            Request::METHOD_GET,
            $this->router->generate('api-oauth2-userinfo', parameters: ['access_token' => $accessToken])
        );

        $this->assertUserInfoResponse($username);
    }

    public function testUserinfoEndpointReturnsUserInfoWithBearerTokenAuthentication(): void
    {
        // testing data
        $clientId = '1234567890';
        $clientSecret = '123456789012345678901234567890';
        $redirectUri = 'https://redirect/uri';
        $clientScopes = OAuth2Scope::EMAIL->value;
        $clientGrants = [OAuth2Grants::PASSWORD];
        $username = self::VALID_USERNAME;
        $password = self::VALID_APP_PASSWORD1;

        $this->prepareMySQL();
        $this->prepareMongoDB(__DIR__ . '/MongoDBFixture1');

        $client = $this->createOAuth2Client($clientId, $clientSecret, $clientScopes, $redirectUri, $clientGrants);

        $accessToken = $this->requestBearerToken($client, $username, $password);

        $this->browserClient->request(
            Request::METHOD_GET,
            $this->router->generate('api-oauth2-userinfo'),
            server: [
                'HTTP_AUTHORIZATION' => sprintf('Bearer %s', $accessToken),
            ]
        );

        $this->assertUserInfoResponse($username);
    }

    public function testUserinfoEndpointReturnsUserPrefersQueryParameterOverBearerAuthentication(): void
    {
        // testing data
        $clientId = '1234567890';
        $clientSecret = '123456789012345678901234567890';
        $redirectUri = 'https://redirect/uri';
        $clientScopes = OAuth2Scope::EMAIL->value;
        $clientGrants = [OAuth2Grants::PASSWORD];
        $username = self::VALID_USERNAME;
        $anotherUsername = self::ANOTHER_VALID_USERNAME;
        $password = self::VALID_APP_PASSWORD1;

        $this->prepareMySQL();
        $this->prepareMongoDB(__DIR__ . '/MongoDBFixture1');

        $client = $this->createOAuth2Client($clientId, $clientSecret, $clientScopes, $redirectUri, $clientGrants);

        $accessToken = $this->requestBearerToken($client, $username, $password);
        $anotherAccessToken = $this->requestBearerToken($client, $anotherUsername, $password);

        $this->browserClient->request(
            Request::METHOD_GET,
            $this->router->generate('api-oauth2-userinfo', ['access_token' => $accessToken]),
            server: [
                'HTTP_AUTHORIZATION' => sprintf('Bearer %s', $anotherAccessToken),
            ]
        );

        $this->assertUserInfoResponse($username);
    }

    /**
     * @dataProvider userinfoThrowsErrorOnInvalidAccessTokenDataProvider
     */
    public function testUserinfoThrowsErrorOnInvalidAccessToken(?string $accessToken, ?string $bearerToken): void
    {
        // testing data
        $clientId = '1234567890';
        $clientSecret = '123456789012345678901234567890';
        $redirectUri = 'https://redirect/uri';
        $clientScopes = OAuth2Scope::EMAIL->value;
        $clientGrants = [OAuth2Grants::PASSWORD];

        $this->prepareMySQL();
        $this->prepareMongoDB(__DIR__ . '/MongoDBFixture1');

        $this->createOAuth2Client($clientId, $clientSecret, $clientScopes, $redirectUri, $clientGrants);

        $this->browserClient->request(
            Request::METHOD_GET,
            $this->router->generate(
                'api-oauth2-userinfo',
                null === $accessToken ? [] : ['access_token' => 'invalid-access-token']
            ),
            server: null === $bearerToken
                ? []
                : ['HTTP_AUTHORIZATION' => sprintf('Bearer %s', $bearerToken)]
        );

        $this->assertResponseStatusCodeSame(401, $this->getName());
    }

    public function userinfoThrowsErrorOnInvalidAccessTokenDataProvider(): Generator
    {
        yield 'invalid access_token' => [
            'access_token' => 'invalid-access-token',
            'bearer token' => null,
        ];

        yield 'invalid Bearer token' => [
            'access_token' => null,
            'bearer token' => 'invalid-access-token',
        ];

        yield 'no token' => [
            'access_token' => null,
            'bearer token' => null,
        ];
    }

    private function requestBearerToken(Client $client, string $username, string $password): string
    {
        // requesting bearer token only works with password grant
        $this->assertNotEmpty(
            array_filter(
                $client->getGrants(),
                fn (Grant $grant): bool => (string) $grant === OAuth2Grants::PASSWORD
            )
        );

        $this->browserClient->request(
            Request::METHOD_POST,
            $this->router->generate('api-oauth2-token'),
            [
                'grant_type' => OAuth2Grants::PASSWORD,
                'client_id' => $client->getIdentifier(),
                'client_secret' => $client->getSecret(),
                'scope' => implode(' ', array_map(fn (Scope $scope): string => (string) $scope, $client->getScopes())),
                'username' => $username,
                'password' => $password,
            ]
        );
        $response = $this->browserClient->getResponse();

        $this->assertResponseTypeJson(200);

        // validate we got the access Bearer token and a refresh token
        $responseArray = json_decode($response->getContent(), true);
        $this->assertJsonContainsSubset(['token_type' => 'Bearer'], $responseArray);
        $this->assertArrayHasKey('access_token', $responseArray);

        return $responseArray['access_token'];
    }

    private function createOAuth2Client(
        string $clientId,
        string $clientSecret,
        string $clientScopes,
        string $redirectUri,
        array $clientGrants
    ): Client {
        // create OAuth2 client record
        $client = new Client('Test client', $clientId, $clientSecret);
        $client->setScopes(...array_map(fn (string $scope) => new Scope($scope), explode(' ', $clientScopes)));
        $client->setActive(true);
        $client->setRedirectUris(new RedirectUri($redirectUri));
        $client->setGrants(...array_map(fn (string $grant) => new Grant($grant), $clientGrants));
        $this->entityManager->persist($client);
        $this->entityManager->flush();

        return $client;
    }

    private function assertAccessTokenResponse(
        string $clientId,
        ?string $username,
        string $scope,
        string $message = ''
    ): void {
        $response = $this->browserClient->getResponse();

        $this->assertResponseTypeJson(200, $message);

        // validate we got the access Bearer token and a refresh token
        $responseArray = json_decode($response->getContent(), true);
        $this->assertJsonContainsSubset(['token_type' => 'Bearer'], $responseArray, $message);
        $this->assertArrayHasKey('access_token', $responseArray, $message);
        $this->assertArrayHasKey('refresh_token', $responseArray, $message);

        // parse JWT and validate its contents
        $jwt = (new Parser(new JoseEncoder()))->parse($responseArray['access_token']);
        $this->assertArrayContainsSubset(['typ' => 'JWT'], $jwt->headers()->all(), $message);
        $this->assertArrayContainsSubset(
            [
                'aud' => [$clientId],
                'sub' => $username ?? '',
                'scopes' => explode(' ', $scope),
                'scope' => $scope
            ],
            $jwt->claims()->all(),
            $message
        );
        $this->assertTrue($jwt->claims()->has('jti'), $message);

        /** @var DateTimeImmutable $issuedAtDate */
        $issuedAtDate = $jwt->claims()->get('iat');
        /** @var DateTimeImmutable $notBeforeDate */
        $notBeforeDate = $jwt->claims()->get('nbf');
        /** @var DateTimeImmutable $expirationDate */
        $expirationDate = $jwt->claims()->get('exp');

        $this->assertInstanceOf(DateTimeImmutable::class, $issuedAtDate, $message);
        $this->assertInstanceOf(DateTimeImmutable::class, $notBeforeDate, $message);
        $this->assertInstanceOf(DateTimeImmutable::class, $expirationDate, $message);

        // check the dates
        $this->assertLessThanOrEqual(time(), $issuedAtDate->getTimestamp());
        $this->assertLessThanOrEqual(time(), $notBeforeDate->getTimestamp());
        $this->assertGreaterThan(time(), $expirationDate->getTimestamp());

        // the dates muset not have microseconds - Dovecot does not know how to parse them - DOP-2753
        $this->assertSame('.000000', $issuedAtDate->format('.u'));
        $this->assertSame('.000000', $notBeforeDate->format('.u'));
        $this->assertSame('.000000', $expirationDate->format('.u'));

        // get the access token identifier for database lookups
        $accessTokenIdentifier = $jwt->claims()->get('jti');

        // check if the access token is also persisted in database - find the single access token
        $persistedAccessTokens = $this->entityManager->getRepository(AccessToken::class)->findAll();
        $this->assertCount(1, $persistedAccessTokens, $message);
        $this->assertInstanceOf(AccessToken::class, $persistedAccessTokens[0], $message);
        /** @var AccessToken $persistedAccessToken */
        $persistedAccessToken = $persistedAccessTokens[0];

        // check if access token has correct values
        $this->assertSame($accessTokenIdentifier, $persistedAccessToken->getIdentifier(), $message);
        $this->assertSame($username, $persistedAccessToken->getUserIdentifier(), $message);
        $this->assertSame($clientId, $persistedAccessToken->getClient()->getIdentifier(), $message);
        $this->assertEquals([new Scope(OAuth2Scope::EMAIL->value)], $persistedAccessToken->getScopes(), $message);
        $this->assertFalse($persistedAccessToken->isRevoked(), $message);

        // check if the refresh token is also persisted in database - find the single refresh token
        $persistedRefreshTokens = $this->entityManager->getRepository(RefreshToken::class)->findAll();
        $this->assertCount(1, $persistedRefreshTokens, $message);
        $this->assertInstanceOf(RefreshToken::class, $persistedRefreshTokens[0], $message);
        /** @var RefreshToken $persistedRefreshToken */
        $persistedRefreshToken = $persistedRefreshTokens[0];

        // check if the refresh token has correct values
        $this->assertSame($accessTokenIdentifier, $persistedRefreshToken->getAccessToken()->getIdentifier(), $message);
        $this->assertFalse($persistedRefreshToken->isRevoked(), $message);
    }

    private function assertResponseTypeJson(int $expectedStatusCode = null, string $message = ''): void
    {
        $response = $this->browserClient->getResponse();

        $this->assertTrue($response->headers->has('Content-Type'), $message);
        $this->assertStringStartsWith('application/json', $response->headers->get('Content-Type'), $message);

        if (null !== $expectedStatusCode) {
            $this->assertResponseStatusCodeSame($expectedStatusCode);
        }
    }

    private function assertUserInfoResponse(string $username): void
    {
        $response = $this->browserClient->getResponse();

        $this->assertResponseTypeJson(200);

        // validate we got the access Bearer token and a refresh token
        $responseArray = json_decode($response->getContent(), true);
        $this->assertJsonContainsSubset(['username' => $username], $responseArray);
    }

    private function assertErrorJsonResponse(string $errorCode, int $httpStatusCode, string $message = ''): void
    {
        $response = $this->browserClient->getResponse();

        $this->assertResponseTypeJson($httpStatusCode, $message);

        $responseArray = json_decode($response->getContent(), true);
        $this->assertJsonContainsSubset(['error' => $errorCode], $responseArray, $message);
    }
}
