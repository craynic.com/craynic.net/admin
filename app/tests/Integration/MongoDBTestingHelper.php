<?php

namespace Tests\App\Integration;

use Doctrine\ODM\MongoDB\DocumentManager;
use FilesystemIterator;
use MongoDB\Driver\BulkWrite;
use PHPUnit\Framework\Exception;
use SplFileInfo;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\StringInput;
use Symfony\Component\HttpKernel\KernelInterface;

final class MongoDBTestingHelper
{
    private bool $isSetup = false;

    public function __construct(private readonly KernelInterface $kernel, private readonly DocumentManager $dm)
    {
    }

    public function setup(?string $fixturesDir = null): void
    {
        $this->isSetup = true;

        $application = new Application($this->kernel);
        $application->setAutoExit(false);
        $application->run(new StringInput('mongodb:migrations:migrate -n -q'));

        if (null !== $fixturesDir) {
            $this->importFixtureFilesToMongoDB($fixturesDir);
        }
    }

    public function teardown(): void
    {
        if (!$this->isSetup) {
            return;
        }

        $this->dm->getClient()->dropDatabase(
            $this->dm->getConfiguration()->getDefaultDB()
        );
        $this->isSetup = false;
    }

    private function importFixtureFilesToMongoDB(string $fixturesDir): void
    {
        $dbName = $this->dm->getConfiguration()->getDefaultDB();
        $database = $this->dm->getClient()->$dbName;

        /** @var SplFileInfo $fileInfo */
        foreach (new FilesystemIterator($fixturesDir) as $fileInfo) {
            if (strcasecmp($fileInfo->getExtension(), 'php') !== 0) {
                continue;
            }

            $documents = require $fileInfo->getRealPath();
            if (!is_array($documents)) {
                throw new Exception('Invalid fixture data - should be an array of documents.');
            }

            $bulkWrite = new BulkWrite();
            foreach ($documents as $document) {
                $bulkWrite->insert($document);
            }

            $collectionName = substr($fileInfo->getBasename($fileInfo->getExtension()), 0, -1);
            $this->dm->getClient()->getManager()->executeBulkWrite(
                sprintf('%s.%s', $database->getDatabaseName(), $collectionName),
                $bulkWrite
            );
        }
    }
}
