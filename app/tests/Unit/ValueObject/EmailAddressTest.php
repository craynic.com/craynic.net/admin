<?php

namespace Tests\App\Unit\ValueObject;

use App\ValueObject\EmailAddress;
use Generator;
use InvalidArgumentException;
use Tests\App\TestCase;

final class EmailAddressTest extends TestCase
{
    /**
     * @dataProvider itWorksForValidEmailAddressesDataProvider
     */
    public function testItWorksForValidEmailAddresses(string $emailAddress): void
    {
        $emailObject = new EmailAddress($emailAddress);

        $this->assertSame($emailAddress, (string) $emailObject, $this->getName());
    }

    public function itWorksForValidEmailAddressesDataProvider(): Generator
    {
        yield 'example 1' => ['test@best.com'];
        yield 'example 2' => ['test@10.10.10.10'];
        yield 'example 3' => ['test.best@domain.family'];
        yield 'example 4' => ['extension+mailbox@domain.tld'];
    }

    /**
     * @dataProvider itThrowsExceptionForInvalidEmailAddressDataProvider
     */
    public function testItThrowsExceptionForInvalidEmailAddress(string $emailAddress): void
    {
        $this->expectException(InvalidArgumentException::class);

        new EmailAddress($emailAddress);
    }

    public function itThrowsExceptionForInvalidEmailAddressDataProvider(): Generator
    {
        yield 'example 1' => ['test@best'];
        yield 'example 3' => ['@domain.family'];
        yield 'example 4' => ['extension+mailbox'];
        yield 'example 5' => [''];
    }

    public function testItLowercasesEmailAddress(): void
    {
        $mailbox = 'Test.Best';
        $domain = 'DOMAIN.FaMiLy';

        $this->assertNotSame(strtolower($mailbox), $mailbox);
        $this->assertNotSame(strtolower($domain), $domain);

        $emailAddress = EmailAddress::fromValues($domain, $mailbox);

        $this->assertSame(strtolower($mailbox), $emailAddress->mailbox);
        $this->assertSame(strtolower($domain), $emailAddress->domain);
    }
}
