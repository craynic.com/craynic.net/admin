<?php

declare(strict_types=1);

use App\EntityManager\MainEntityManager;
use App\EntityManager\ReadModelsEntityManager;
use App\Security\Voter\MailboxAdminWithTwoFactorAuthVoter;
use App\Service\AccessTokenRepository;
use App\Service\QuotaWeeklyReporter;
use App\Service\ReadModel\ReadModel;
use App\Service\ReadModelRefresher;
use App\Service\SystemTimeProvider;
use App\Service\TimeProvider;
use App\Service\TwigGlobals;
use App\Service\TwoFactorAuth\Persister;
use App\Service\TwoFactorAuth\BackupCodesManager;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\ResponseFactoryInterface;
use Symfony\Bridge\PsrHttpMessage\HttpFoundationFactoryInterface;
use Symfony\Bridge\PsrHttpMessage\HttpMessageFactoryInterface;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Component\HttpFoundation\Session\Storage\Handler\PdoSessionHandler;

use function Symfony\Component\DependencyInjection\Loader\Configurator\env;
use function Symfony\Component\DependencyInjection\Loader\Configurator\param;
use function Symfony\Component\DependencyInjection\Loader\Configurator\service;
use function Symfony\Component\DependencyInjection\Loader\Configurator\tagged_iterator;

return function (ContainerConfigurator $configurator): void {
    $enabledLocales = ['en', 'cs'];

    # we need to set the value here in compile time due to an open bug:
    # https://github.com/symfony/symfony/issues/40906
    $trustedHeadersRawValue = getenv('APP_PROXY_TRUSTED_HEADERS');
    $trustedHeadersValue = trim($trustedHeadersRawValue) === ''
        ? []
        : array_map(fn (string $value): string => trim($value), explode(',', $trustedHeadersRawValue));

    $configurator
        ->parameters()
        ->set('app.locale.supported_locales', $enabledLocales)
        ->set('app.locale.default', $enabledLocales[array_key_first($enabledLocales)])
        ->set('app.locale.route_requirements', implode('|', $enabledLocales))
        ->set('app.secret', env('APP_SECRET'))
        ->set('app.sql_dsn', env('APP_DB_DSN'))
        ->set('app.sql_dsn_readmodels', env('APP_DB_DSN_READMODELS'))
        ->set('env(APP_MONGO_CONN_URI_WITH_APPNAME)', '')
        ->set('app.mongo.conn_uri', env('APP_MONGO_CONN_URI_WITH_APPNAME'))
        ->set('app.mongo.db', env('APP_MONGO_DB'))
        ->set('app.oauth_defuse_key', env('APP_OAUTH_ENCRYPTION_DEFUSE_KEY'))
        ->set('app.webmail.url', env('APP_WEBMAIL_URL'))
        ->set('app.proxy.trusted_proxies', env('APP_PROXY_TRUSTED_PROXIES'))
        ->set('app.proxy.trusted_headers', $trustedHeadersValue)
        ->set('app.validation.password.min_strength', 4)
        ->set('app.validation.password.min_length', 10)
        ->set('app.validation.password.not_compromised_password_enabled', true)
        ->set('env(APP_MAILER_DSN)', 'native://default')
        ->set('app.mailer.dsn', env('APP_MAILER_DSN'))
        ->set('env(APP_LOG_LEVEL)', 'info')
        ->set('app.log.level', env('APP_LOG_LEVEL'))
        ->set('app.quota.reporting.sender', env('APP_QUOTA_REPORTING_SENDER'))
        ->set('app.2fa.trusted_device.encryption_key', env('APP_2FA_TRUSTED_DEVICE_ENCRYPTION_KEY'))
        ->set('app.2fa.google.server_name', env('APP_2FA_GOOGLE_SERVER_NAME'))
        ->set('app.2fa.google.issuer', env('APP_2FA_GOOGLE_ISSUER'));

    $services = $configurator->services()->defaults()
        ->autowire()
        ->autoconfigure();

    $services
        ->load('App\\', '%kernel.project_dir%/src/*')
        ->exclude('%kernel.project_dir%/src/{Entity,Kernel.php}');

    $services
        ->set('league.oauth2_server.repository.access_token', AccessTokenRepository::class)
        ->alias(HttpMessageFactoryInterface::class, 'league.oauth2_server.factory.psr_http')
        ->alias(ResponseFactoryInterface::class, 'league.oauth2_server.factory.psr17')
        ->alias(HttpFoundationFactoryInterface::class, 'league.oauth2_server.factory.http_foundation')
        ->alias(TimeProvider::class, SystemTimeProvider::class);

    $services
        ->instanceof(ReadModel::class)
        ->tag('app.readmodel');

    $services
        ->instanceof(MailboxAdminWithTwoFactorAuthVoter::class)
        ->tag('security.voter');

    $services
        ->set(ReadModelRefresher::class)
        ->args([tagged_iterator('app.readmodel')]);

    $services
        ->set('app.twigGlobals', TwigGlobals::class);
    $services
        ->get('app.twigGlobals')
        ->arg('$webmailURL', param('app.webmail.url'));

    $services
        ->set(MainEntityManager::class)
        ->bind(EntityManagerInterface::class, service('doctrine.orm.main_entity_manager'));

    $services
        ->set(ReadModelsEntityManager::class)
        ->bind(EntityManagerInterface::class, service('doctrine.orm.readmodels_entity_manager'));

    $services
        ->set('app.pdo_session_handler', PdoSessionHandler::class)
        ->args([
            param('app.sql_dsn'),
            [
                'db_connection_options' => [
                    PDO::MYSQL_ATTR_SSL_CA => true,
                    PDO::MYSQL_ATTR_SSL_VERIFY_SERVER_CERT => false,
                ]
            ]
        ]);

    $services
        ->get(QuotaWeeklyReporter::class)
        ->arg('$quotaReportSender', param('app.quota.reporting.sender'));

    $services
        ->set('app.2fa.persister', Persister::class);

    $services
        ->set('app.2fa.backup_codes_manager', BackupCodesManager::class);
};
