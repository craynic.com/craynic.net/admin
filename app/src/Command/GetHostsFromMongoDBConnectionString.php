<?php

declare(strict_types=1);

namespace App\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    'app:get-hosts-from-mongodb-connection-string',
    'Parses hosts from MongoDB connection string and prints them on separate lines'
)]
final class GetHostsFromMongoDBConnectionString extends Command
{
    private const int MONGODB_DEFAULT_PORT = 27017;

    public function configure(): void
    {
        $this->addArgument('mongoDbConnectionString', InputArgument::REQUIRED, 'MongoDB connection string');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $connectionString = strtolower($input->getArgument('mongoDbConnectionString'));

        $regEx =
            '/^mongodb(?:\+srv)?:\/\/(?:[^@:]+(?::[^@]+)?@)?((?:[^\/,@:]+(?::\d+)?,)*[^\/,@:]+(?::\d+)?)(?:\/.*)?$/';

        $matches = [];
        if (!preg_match($regEx, $connectionString, $matches)) {
            $output->writeln('Invalid connection string.');
            return self::FAILURE;
        }

        foreach (explode(',', $matches[1]) as $hostName) {
            if (trim($hostName) !== '') {
                $output->writeln(str_contains($hostName, ':')
                    ? $hostName
                    : sprintf('%s:%d', $hostName, self::MONGODB_DEFAULT_PORT));
            }
        }

        return self::SUCCESS;
    }
}
