<?php

declare(strict_types=1);

namespace App\Command;

use App\DocumentExporter\MailRedirectExporter;
use App\Repository\MailDomainRepository;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand('app:domains:redirects:get', 'Get a redirect by its e-mail address or by its mailbox name and domain UUID')]
final class DomainsRedirectsGet extends Command
{
    public function __construct(
        private readonly MailDomainRepository $mailDomainRepository,
        private readonly LoggerInterface $logger,
        private readonly MailRedirectExporter $redirectExporter,
        string $name = null
    ) {
        parent::__construct($name);
    }

    public function configure(): void
    {
        $this->addArgument('redirectIdentifier', InputArgument::REQUIRED, 'E-mail address or UUID of the redirect');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $redirectIdentifier = $input->getArgument('redirectIdentifier');

        $this->logger->debug(sprintf('Getting redirect %s...', $redirectIdentifier));

        $redirect = $this->mailDomainRepository
            ->findOneByRedirectIdentifier($redirectIdentifier)
            ->getRedirectByIdentifier($redirectIdentifier);

        $output->writeln(yaml_emit($this->redirectExporter->exportDetail($redirect)));

        return self::SUCCESS;
    }
}
