<?php

declare(strict_types=1);

namespace App\Command;

use App\Document\Quota;
use App\Exception\MongoDBChangeStreamInvalidated;
use App\Service\QuotaUsageUpdater;
use Doctrine\ODM\MongoDB\DocumentManager;
use MongoDB\ChangeStream;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand('app:update-quota-usage', 'Updates quota usage from Dovecot\'s database.')]
final class UpdateQuotaUsage extends Command
{
    private const int WATCH_MODE_EXECUTION_DELAY = 5;
    private const int QUOTA_WATCH_MODE_EXECUTION_DELAY_MS = 10;

    public function __construct(
        private readonly QuotaUsageUpdater $quotaUsageUpdater,
        private readonly LoggerInterface $logger,
        private readonly DocumentManager $documentManager,
        string $name = null
    ) {
        parent::__construct($name);
    }

    protected function configure(): void
    {
        $this
            ->addOption('watch', 'w', InputOption::VALUE_NONE, 'Whether to start watch mode after initial update');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        return $input->getOption('watch')
            ? $this->executeWatchMode()
            : $this->executeOneTimeRun();
    }

    private function executeOneTimeRun(): int
    {
        $this->quotaUsageUpdater->update();

        return self::SUCCESS;
    }

    private function executeWatchMode(): int
    {
        $collection = $this->documentManager->getDocumentCollection(Quota::class);
        $changeStream = $collection->watch(options: ['maxAwaitTimeMS' => self::QUOTA_WATCH_MODE_EXECUTION_DELAY_MS]);
        $changeStream->rewind();

        $this->executeOneTimeRun();

        $this->logger->info('Started watching quotas for changes...');

        try {
            while (true) {
                sleep(self::WATCH_MODE_EXECUTION_DELAY);

                if ($this->hasChangeStreamChanged($changeStream)) {
                    $this->documentManager->getRepository(Quota::class)->clear();
                }

                $this->executeOneTimeRun();
            }
        } catch (MongoDBChangeStreamInvalidated) {
            return self::SUCCESS;
        }
    }

    private function hasChangeStreamChanged(ChangeStream $changeStream): bool
    {
        $changed = false;

        $changeStream->next();
        while ($changeStream->valid()) {
            $event = $changeStream->current();
            if ($event['operationType'] === 'invalidate') {
                throw new MongoDBChangeStreamInvalidated();
            }

            $changed = true;
            $changeStream->next();
        }

        return $changed;
    }
}
