<?php

declare(strict_types=1);

namespace App\Command;

use App\DocumentExporter\MailRedirectExporter;
use App\Repository\MailDomainRepository;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand('app:domains:redirects:list', 'List redirects for a given domain')]
final class DomainsRedirectsList extends Command
{
    public function __construct(
        private readonly MailDomainRepository $mailDomainRepository,
        private readonly LoggerInterface $logger,
        private readonly MailRedirectExporter $redirectExporter,
        string $name = null
    ) {
        parent::__construct($name);
    }

    public function configure(): void
    {
        $this->addArgument('domainIdentifier', InputArgument::REQUIRED, 'Domain name or UUID of the domain');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $domainIdentifier = $input->getArgument('domainIdentifier');

        $this->logger->debug(sprintf('Getting redirects for domain %s...', $domainIdentifier));

        $domain = $this->mailDomainRepository->findOneByDomainIdentifier($domainIdentifier);

        $output->writeln(yaml_emit($this->redirectExporter->exportList(...$domain->getRedirects())));

        return self::SUCCESS;
    }
}
