<?php

declare(strict_types=1);

namespace App\Command;

use App\DocumentExporter\MailDomainExporter;
use App\Repository\MailDomainRepository;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand('app:domains:aliases:get', 'Get an alias by its domain name')]
final class DomainsAliasesGet extends Command
{
    public function __construct(
        private readonly MailDomainRepository $mailDomainRepository,
        private readonly MailDomainExporter $domainExporter,
        private readonly LoggerInterface $logger,
        string $name = null
    ) {
        parent::__construct($name);
    }

    public function configure(): void
    {
        $this->addArgument('aliasIdentifier', InputArgument::REQUIRED, 'Domain name of the alias');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $aliasIdentifier = strtolower($input->getArgument('aliasIdentifier'));

        $this->logger->debug(sprintf('Getting domain alias "%s"...', $aliasIdentifier));

        $domain = $this->mailDomainRepository->findOneByDomainAlias($aliasIdentifier);
        $alias = $domain->getAliasByIdentifier($aliasIdentifier);

        $output->writeln(yaml_emit($this->domainExporter->exportSingleAlias($domain, $alias)));

        return self::SUCCESS;
    }
}
