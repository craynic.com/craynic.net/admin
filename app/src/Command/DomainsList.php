<?php

declare(strict_types=1);

namespace App\Command;

use App\DocumentExporter\MailDomainExporter;
use App\Repository\MailDomainRepository;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand('app:domains:list', 'List all the domains')]
final class DomainsList extends Command
{
    public function __construct(
        private readonly MailDomainRepository $mailDomainRepository,
        private readonly MailDomainExporter $mailDomainExporter,
        private readonly LoggerInterface $logger,
        string $name = null
    ) {
        parent::__construct($name);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->logger->debug('Getting all domains...');

        $output->writeln(yaml_emit($this->mailDomainExporter->exportList(
            ...$this->mailDomainRepository->findAll()
        )));

        return self::SUCCESS;
    }
}
