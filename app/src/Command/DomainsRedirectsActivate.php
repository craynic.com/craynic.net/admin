<?php

declare(strict_types=1);

namespace App\Command;

use App\DocumentExporter\MailRedirectExporter;
use App\Repository\MailDomainRepository;
use Doctrine\ODM\MongoDB\DocumentManager;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand('app:domains:redirects:activate', 'Activate a redirect')]
final class DomainsRedirectsActivate extends Command
{
    public function __construct(
        private readonly DocumentManager $documentManager,
        private readonly MailDomainRepository $mailDomainRepository,
        private readonly MailRedirectExporter $redirectExporter,
        string $name = null
    ) {
        parent::__construct($name);
    }

    public function configure(): void
    {
        $this->addArgument('redirectIdentifier', InputArgument::REQUIRED, 'E-mail address or UUID of the redirect');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $redirectIdentifier = $input->getArgument('redirectIdentifier');

        $domain = $this->mailDomainRepository->findOneByRedirectIdentifier($redirectIdentifier);

        $domain->activateRedirect($redirectIdentifier);

        $this->documentManager->flush();

        $output->writeln(yaml_emit($this->redirectExporter->exportDetail(
            $domain->getRedirectByIdentifier($redirectIdentifier)
        )));

        return self::SUCCESS;
    }
}
