<?php

declare(strict_types=1);

namespace App\Command;

use App\DocumentExporter\MailDkimExporter;
use App\Repository\MailDomainRepository;
use Doctrine\ODM\MongoDB\DocumentManager;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand('app:domains:dkim:disable', 'Disable DKIM on a given domain')]
final class DomainsDkimDisable extends Command
{
    public function __construct(
        private readonly MailDomainRepository $mailDomainRepository,
        private readonly DocumentManager $documentManager,
        private readonly MailDkimExporter $mailDkimExporter,
        string $name = null
    ) {
        parent::__construct($name);
    }

    public function configure(): void
    {
        $this->addArgument(
            'domainIdentifier',
            InputArgument::REQUIRED,
            'UUID or a name of the domain or its aliases to get'
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $domainIdentifier = $input->getArgument('domainIdentifier');

        $domain = $this->mailDomainRepository->findOneByDomainIdentifier($domainIdentifier);
        $domain->disableDkim();

        $this->documentManager->persist($domain);
        $this->documentManager->flush();

        $output->writeln(yaml_emit($this->mailDkimExporter->exportDetail($domain->getDkim())));

        return self::SUCCESS;
    }
}
