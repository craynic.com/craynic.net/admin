<?php

declare(strict_types=1);

namespace App\Command;

use App\DocumentExporter\MailDkimExporter;
use App\Exception\DkimNotConfigured;
use App\Repository\MailDomainRepository;
use App\Service\DkimManager;
use Doctrine\ODM\MongoDB\DocumentManager;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;

#[AsCommand('app:domains:dkim:generate', 'Generate new DNS DKIM record for a given domain')]
final class DomainsDkimGenerate extends Command
{
    public function __construct(
        private readonly MailDomainRepository $mailDomainRepository,
        private readonly DocumentManager $documentManager,
        private readonly DkimManager $DKIMManager,
        private readonly MailDkimExporter $dkimExporter,
        string $name = null
    ) {
        parent::__construct($name);
    }

    public function configure(): void
    {
        $this->addArgument(
            'domainIdentifier',
            InputArgument::REQUIRED,
            'UUID or a name of the domain or its aliases to get'
        );

        $this->addOption(
            'yes',
            null,
            InputOption::VALUE_NONE,
            'Assume "yes" as an answer whether to overwrite existing DKIM key'
        );

        $this->addOption(
            'enable',
            null,
            InputOption::VALUE_NONE,
            'Enable the newly generated DKIM key'
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $domainIdentifier = $input->getArgument('domainIdentifier');

        $domain = $this->mailDomainRepository->findOneByDomainIdentifier($domainIdentifier);

        try {
            $domain->assertDkimIsConfigured();

            if (!$input->getOption('yes')) {
                $helper = $this->getHelper('question');
                $question = new ConfirmationQuestion(sprintf(
                    '<info>%s [y/n]</info> ',
                    sprintf(
                        'The domain "%s" has already a DKIM record. Are you sure you want to generate a new one?',
                        $domainIdentifier
                    )
                ));

                if (!$helper->ask($input, $output, $question)) {
                    return Command::SUCCESS;
                }
            }
        } catch (DkimNotConfigured) {
        }

        $domain->setDkim($this->DKIMManager->generateNew());

        if ($input->getOption('enable')) {
            $domain->enableDkim();
        }

        $this->documentManager->flush();

        $output->writeln(yaml_emit($this->dkimExporter->exportDetail($domain->getDkim())));

        return self::SUCCESS;
    }
}
