<?php

declare(strict_types=1);

namespace App\Command;

use App\DocumentExporter\MailDomainExporter;
use App\Repository\MailDomainRepository;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand('app:domains:aliases:list', 'List aliases for a given domain')]
final class DomainsAliasesList extends Command
{
    public function __construct(
        private readonly MailDomainRepository $mailDomainRepository,
        private readonly MailDomainExporter $domainExporter,
        private readonly LoggerInterface $logger,
        string $name = null
    ) {
        parent::__construct($name);
    }

    public function configure(): void
    {
        $this->addArgument('domainIdentifier', InputArgument::REQUIRED, 'Domain name or UUID of the domain');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $domainIdentifier = $input->getArgument('domainIdentifier');

        $this->logger->debug(sprintf('Getting aliases for domain %s...', $domainIdentifier));

        $domain = $this->mailDomainRepository->findOneByDomainIdentifier($domainIdentifier);

        $output->writeln(yaml_emit($this->domainExporter->exportAliasList(
            $domain,
            ...$domain->getAliases()->toArray()
        )));

        return self::SUCCESS;
    }
}
