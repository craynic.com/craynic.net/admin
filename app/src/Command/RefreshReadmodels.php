<?php

declare(strict_types=1);

namespace App\Command;

use App\Document\MailDomain;
use App\Document\Quota;
use App\Exception\MongoDBChangeStreamInvalidated;
use App\Service\ReadModelRefresher;
use App\Service\TimeProvider;
use Doctrine\ODM\MongoDB\DocumentManager;
use MongoDB\ChangeStream;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand('app:refresh-readmodels', 'Refreshes the readmodels')]
final class RefreshReadmodels extends Command
{
    private const int WATCH_MODE_EXECUTION_DELAY = 5;
    private const int WATCH_MODE_EXECUTION_DELAY_MS = self::WATCH_MODE_EXECUTION_DELAY * 1_000;
    private const int QUOTA_WATCH_MODE_EXECUTION_DELAY_MS = 10;

    public function __construct(
        private readonly ReadModelRefresher $readModelRefresher,
        private readonly DocumentManager $documentManager,
        private readonly LoggerInterface $logger,
        private readonly TimeProvider $timeProvider,
        string $name = null
    ) {
        parent::__construct($name);
    }

    protected function configure(): void
    {
        $this
            ->addOption('watch', 'w', InputOption::VALUE_NONE, 'Whether to start watch mode after initial refresh');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        return !$input->getOption('watch')
            ? $this->executeOneTimeRun()
            : $this->executeWatchMode();
    }

    private function executeOneTimeRun(): int
    {
        $this->logger->info('Refreshing all readmodels...');

        $this->readModelRefresher->refresh();

        $this->logger->info('Refreshing readmodels finished.');

        return self::SUCCESS;
    }

    private function executeWatchMode(): int
    {
        $domainsCollection = $this->documentManager->getDocumentCollection(MailDomain::class);
        $domainsChangeStream = $domainsCollection->watch(
            options: ['maxAwaitTimeMS' => self::WATCH_MODE_EXECUTION_DELAY_MS]
        );

        $quotaCollection = $this->documentManager->getDocumentCollection(Quota::class);
        $quotaChangeStream = $quotaCollection->watch(
            options: ['maxAwaitTimeMS' => self::QUOTA_WATCH_MODE_EXECUTION_DELAY_MS]
        );
        $quotaChangeStream->rewind();

        $this->logger->info('Started watching MongoDB collection for changes...');

        $this->logger->info('Running initial readmodels refresh...');
        $this->readModelRefresher->refresh();
        $this->logger->info('Initial readmodels refresh done.');

        for ($domainsChangeStream->rewind(); true; $domainsChangeStream->next()) {
            try {
                $quotaChangeStreamChanged = $this->hasChangeStreamChanged($quotaChangeStream);
            } catch (MongoDBChangeStreamInvalidated) {
                return self::SUCCESS;
            }

            if ($domainsChangeStream->valid()) {
                $event = $domainsChangeStream->current();
                if ($event['operationType'] === 'invalidate') {
                    return self::SUCCESS;
                }
            } elseif (!$quotaChangeStreamChanged) {
                continue;
            }

            $this->logger->info(sprintf(
                'MongoDB collection changed, waiting %s ms for other changes...',
                number_format(self::WATCH_MODE_EXECUTION_DELAY_MS, 0, ',', '.')
            ));

            $startTime = $this->timeProvider->getCurrentMicrotime();

            // in case the event was not triggered by the domains change stream, wait for the next change
            if (!$domainsChangeStream->valid()) {
                $domainsChangeStream->next();
            }

            // keep waiting for changes until we reach the time limit
            while (
                $domainsChangeStream->valid()
                && ($this->timeProvider->getCurrentMicrotime() - self::WATCH_MODE_EXECUTION_DELAY < $startTime)
            ) {
                $domainsChangeStream->next();
            }

            $this->logger->info('Refreshing readmodels...');
            $this->readModelRefresher->refresh();
            $this->logger->info('Refreshing readmodels done.');
        }
    }

    private function hasChangeStreamChanged(ChangeStream $changeStream): bool
    {
        $changed = false;

        $changeStream->next();
        while ($changeStream->valid()) {
            $event = $changeStream->current();
            if ($event['operationType'] === 'invalidate') {
                throw new MongoDBChangeStreamInvalidated();
            }

            $changed = true;
            $changeStream->next();
        }

        return $changed;
    }
}
