<?php

declare(strict_types=1);

namespace App\Repository;

use App\Document\Quota;
use App\Exception\QuotaNotFound;
use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use Doctrine\Bundle\MongoDBBundle\Repository\ServiceDocumentRepository;
use Doctrine\ODM\MongoDB\Repository\DocumentRepository;

/**
 * @extends DocumentRepository<Quota>
 */
final class QuotaRepository extends ServiceDocumentRepository
{
    public function __construct(ManagerRegistry $managerRegistry)
    {
        parent::__construct($managerRegistry, Quota::class);
    }

    public function findOneByDomain(string $domainName): Quota
    {
        return $this->findOneBy(['domains.domain' => strtolower($domainName)])
            ?? throw new QuotaNotFound($domainName);
    }
}
