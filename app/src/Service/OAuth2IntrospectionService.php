<?php

declare(strict_types=1);

namespace App\Service;

use App\Exception\AccessTokenNotFound;
use App\Security\OAuth2UserInterface;
use JetBrains\PhpStorm\ArrayShape;
use League\Bundle\OAuth2ServerBundle\Manager\AccessTokenManagerInterface;
use League\Bundle\OAuth2ServerBundle\Security\Authentication\Token\OAuth2Token;
use LogicException;

final readonly class OAuth2IntrospectionService
{
    public function __construct(private AccessTokenManagerInterface $accessTokenManager)
    {
    }

    #[ArrayShape([
        'username' => "string",
        'active' => "mixed",
        'scope' => "string",
        'client_id' => "string",
        'exp' => "int"
    ])] public function getIntrospectionDataFromToken(OAuth2Token $token): array
    {
        $loggedUser = $token->getUser();
        if (!$loggedUser instanceof OAuth2UserInterface) {
            throw new LogicException('Invalid user type for OAuth2 token');
        }

        $accessToken = $this->accessTokenManager->find($token->getCredentials());
        if (is_null($accessToken)) {
            throw new AccessTokenNotFound();
        }

        return [
            'username' => $loggedUser->getUserIdentifier(),
            'active' => $loggedUser->isActive(),
            'scope' => implode(' ', $token->getScopes()),
            'client_id' => $token->getOAuthClientId(),
            'exp' => $accessToken->getExpiry()->getTimestamp(),
        ];
    }
}
