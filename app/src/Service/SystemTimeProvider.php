<?php

namespace App\Service;

use DateTimeImmutable;
use DateTimeInterface;

final class SystemTimeProvider implements TimeProvider
{
    public function getCurrentTime(): DateTimeInterface
    {
        return new DateTimeImmutable();
    }

    public function getCurrentMicrotime(): float
    {
        return (float) microtime(true);
    }
}
