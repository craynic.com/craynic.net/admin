<?php

declare(strict_types=1);

namespace App\Service\ReadModel;

use App\Entity\ReadModel\PostfixVirtualMailboxDomain;
use App\EntityManager\ReadModelsEntityManager;
use App\ValueObject\ReadModelEntityCollection;
use Generator;

final class PostfixVirtualMailboxDomainReadModel extends AbstractReadModel
{
    public function __construct(ReadModelsEntityManager $entityManager)
    {
        parent::__construct($entityManager, PostfixVirtualMailboxDomain::class);
    }

    protected function getAllDesiredEntities(ReadModelEntityCollection $existingRecords, array $domains): Generator
    {
        foreach ($domains as $domain) {
            if (!$domain->isActive() || !$domain->isDeliveryEnabled()) {
                continue;
            }

            $newEntity = new PostfixVirtualMailboxDomain($domain->getName());
            yield $existingRecords->get($newEntity) ?? $newEntity;
        }
    }
}
