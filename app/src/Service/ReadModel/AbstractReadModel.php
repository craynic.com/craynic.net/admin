<?php

declare(strict_types=1);

namespace App\Service\ReadModel;

use App\Document\MailDomain;
use App\EntityManager\ReadModelsEntityManager;
use App\ValueObject\ReadModelEntityCollection;
use Doctrine\ORM\EntityRepository;
use Generator;
use LogicException;

/**
 * @template T
 */
abstract class AbstractReadModel implements ReadModel
{
    /** @var EntityRepository<T> */
    protected readonly EntityRepository $repository;

    /** @var string[] */
    protected readonly array $identifier;

    public function __construct(
        protected readonly ReadModelsEntityManager $entityManager,
        private readonly string $entityClassName
    ) {
        $this->repository = $this->entityManager->getRepository($this->entityClassName);

        $meta = $this->entityManager->getClassMetadata($this->entityClassName);
        $this->identifier = $meta->getIdentifierFieldNames();
    }

    public function refresh(array $domains): void
    {
        /** @psalm-var ReadModelEntityCollection<T> $entityCollection */
        $entityCollection = new ReadModelEntityCollection($this->entityManager, $this->entityClassName);
        $entityCollection->add(...$this->repository->findAll());

        foreach ($this->getAllDesiredEntities($entityCollection, $domains) as $desiredRecord) {
            if (!$desiredRecord instanceof $this->entityClassName) {
                throw new LogicException(sprintf('Invalid type of entity %s', $desiredRecord::class));
            }

            $this->entityManager->persist($desiredRecord);
            $entityCollection->remove($desiredRecord);
        }

        foreach ($entityCollection as $redundantRecord) {
            $this->entityManager->remove($redundantRecord);
        }

        $this->entityManager->flush();
        $this->entityManager->clear();
    }

    /**
     * @param ReadModelEntityCollection<T> $existingRecords
     * @param MailDomain[] $domains
     * @return Generator<T>
     */
    abstract protected function getAllDesiredEntities(
        ReadModelEntityCollection $existingRecords,
        array $domains
    ): Generator;
}
