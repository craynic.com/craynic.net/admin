<?php

declare(strict_types=1);

namespace App\Service\ReadModel;

use App\Entity\ReadModel\PostfixSenderLogin;
use App\EntityManager\ReadModelsEntityManager;
use App\ValueObject\ReadModelEntityCollection;
use Generator;

final class PostfixSenderLoginReadModel extends AbstractReadModel
{
    public function __construct(ReadModelsEntityManager $entityManager)
    {
        parent::__construct($entityManager, PostfixSenderLogin::class);
    }

    protected function getAllDesiredEntities(ReadModelEntityCollection $existingRecords, array $domains): Generator
    {
        foreach ($domains as $domain) {
            if (!$domain->isActive() || !$domain->isLoginEnabled()) {
                continue;
            }

            foreach ($domain->getMailboxes() as $mailbox) {
                if (!$mailbox->isActive() || !$mailbox->isLoginEnabled()) {
                    continue;
                }

                $mailboxEmailAddress = sprintf('%s@%s', $mailbox->getName(), $domain->getName());

                $newEntity = new PostfixSenderLogin($mailboxEmailAddress, $mailboxEmailAddress);
                yield $existingRecords->get($newEntity) ?? $newEntity;

                // TODO: allow to send on behalf of mailboxes' aliases, domainaliases etc.

                foreach ($mailbox->getExtraSenders() as $extraSender) {
                    if (!$extraSender->isActive()) {
                        continue;
                    }

                    $newEntity = new PostfixSenderLogin($mailboxEmailAddress, $extraSender->getSender());
                    yield $existingRecords->get($newEntity) ?? $newEntity;
                }
            }
        }
    }
}
