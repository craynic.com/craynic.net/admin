<?php

declare(strict_types=1);

namespace App\Service\ReadModel;

use App\Entity\ReadModel\PostfixVirtualAliasDomain;
use App\EntityManager\ReadModelsEntityManager;
use App\ValueObject\ReadModelEntityCollection;
use Generator;

final class PostfixVirtualAliasDomainReadModel extends AbstractReadModel
{
    public function __construct(ReadModelsEntityManager $entityManager)
    {
        parent::__construct($entityManager, PostfixVirtualAliasDomain::class);
    }

    protected function getAllDesiredEntities(ReadModelEntityCollection $existingRecords, array $domains): Generator
    {
        foreach ($domains as $domain) {
            if (!$domain->isActive() || !$domain->isDeliveryEnabled()) {
                continue;
            }

            foreach ($domain->getAliases() as $alias) {
                if (!$alias->isActive() || !$alias->isDeliveryEnabled()) {
                    continue;
                }

                $newEntity = new PostfixVirtualAliasDomain($alias->getDomain(), $domain->getName());
                yield $existingRecords->get($newEntity) ?? $newEntity;
            }
        }
    }
}
