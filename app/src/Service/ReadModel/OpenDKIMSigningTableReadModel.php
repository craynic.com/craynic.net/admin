<?php

declare(strict_types=1);

namespace App\Service\ReadModel;

use App\Entity\ReadModel\OpenDKIMSigningTable;
use App\EntityManager\ReadModelsEntityManager;
use App\Exception\DkimNotConfigured;
use App\ValueObject\ReadModelEntityCollection;
use Generator;

final class OpenDKIMSigningTableReadModel extends AbstractReadModel
{
    public function __construct(ReadModelsEntityManager $entityManager)
    {
        parent::__construct($entityManager, OpenDKIMSigningTable::class);
    }

    protected function getAllDesiredEntities(ReadModelEntityCollection $existingRecords, array $domains): Generator
    {
        foreach ($domains as $domain) {
            try {
                if (!$domain->isActive() || !$domain->getDkim()->isEnabled()) {
                    continue;
                }
            } catch (DkimNotConfigured) {
                continue;
            }

            $newEntity = new OpenDKIMSigningTable($domain->getName());
            yield $existingRecords->get($newEntity) ?? $newEntity;

            foreach ($domain->getAliases() as $domainAlias) {
                if (!$domainAlias->isActive()) {
                    continue;
                }

                $newEntity = new OpenDKIMSigningTable($domainAlias->getDomain());
                yield $existingRecords->get($newEntity) ?? $newEntity;
            }
        }
    }
}
