<?php

declare(strict_types=1);

namespace App\Service\ReadModel;

use App\Entity\ReadModel\PostfixOriginatingDomain;
use App\EntityManager\ReadModelsEntityManager;
use App\ValueObject\ReadModelEntityCollection;
use Generator;

final class PostfixOriginatingDomainReadModel extends AbstractReadModel
{
    public function __construct(ReadModelsEntityManager $entityManager)
    {
        parent::__construct($entityManager, PostfixOriginatingDomain::class);
    }

    protected function getAllDesiredEntities(ReadModelEntityCollection $existingRecords, array $domains): Generator
    {
        foreach ($domains as $domain) {
            if (!$domain->isActive()) {
                continue;
            }

            $newEntity = new PostfixOriginatingDomain($domain->getName());
            yield $existingRecords->get($newEntity) ?? $newEntity;

            foreach ($domain->getAliases() as $alias) {
                if (!$alias->isActive()) {
                    continue;
                }

                $newEntity = new PostfixOriginatingDomain($alias->getDomain());
                yield $existingRecords->get($newEntity) ?? $newEntity;
            }
        }
    }
}
