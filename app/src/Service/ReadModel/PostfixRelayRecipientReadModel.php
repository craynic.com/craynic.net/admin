<?php

declare(strict_types=1);

namespace App\Service\ReadModel;

use App\Document\MailDomain;
use App\Entity\ReadModel\PostfixRelayRecipient;
use App\EntityManager\ReadModelsEntityManager;
use App\ValueObject\ReadModelEntityCollection;
use Generator;

final class PostfixRelayRecipientReadModel extends AbstractReadModel
{
    public function __construct(ReadModelsEntityManager $entityManager)
    {
        parent::__construct($entityManager, PostfixRelayRecipient::class);
    }

    protected function getAllDesiredEntities(ReadModelEntityCollection $existingRecords, array $domains): Generator
    {
        foreach ($domains as $domain) {
            foreach ($this->getActiveRelayMailboxesFromMailDomain($domain) as $mailboxName) {
                foreach ($this->getActiveRelayDomainsFromMailDomain($domain) as $relayDomainName) {
                    $newEntity = new PostfixRelayRecipient(sprintf('%s@%s', $mailboxName, $relayDomainName));
                    yield $existingRecords->get($newEntity) ?? $newEntity;
                }
            }
        }
    }

    private function getActiveRelayMailboxesFromMailDomain(MailDomain $domain): Generator
    {
        foreach ($domain->getMailboxes() as $mailbox) {
            if (!$mailbox->isActive() || !$mailbox->isDeliveryEnabled()) {
                continue;
            }

            yield $mailbox->getName();
        }

        foreach ($domain->getRedirects() as $redirect) {
            if (!$redirect->isActive()) {
                continue;
            }

            yield $redirect->getMailbox();
        }
    }

    private function getActiveRelayDomainsFromMailDomain(MailDomain $domain): Generator
    {
        if (!$domain->isActive() || !$domain->isDeliveryEnabled()) {
            return;
        }

        yield $domain->getName();

        foreach ($domain->getAliases() as $alias) {
            if (!$alias->isActive() || !$alias->isDeliveryEnabled()) {
                continue;
            }

            yield $alias->getDomain();
        }
    }
}
