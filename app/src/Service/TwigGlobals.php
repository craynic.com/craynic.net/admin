<?php

/** @noinspection PhpUnused */

declare(strict_types=1);

namespace App\Service;

use Scheb\TwoFactorBundle\Model\Google\TwoFactorInterface;
use Symfony\Bundle\SecurityBundle\Security;

final readonly class TwigGlobals
{
    public function __construct(public string $webmailURL, private Security $security)
    {
    }

    public function getLoggedInUser(): ?string
    {
        return $this->security->getUser()?->getUserIdentifier();
    }

    public function isTwoFactorAuthenticationMissing(): bool
    {
        $user = $this->security->getUser();

        return $user instanceof TwoFactorInterface
            && !$user->isGoogleAuthenticatorEnabled();
    }
}
