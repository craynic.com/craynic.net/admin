<?php

namespace App\Service;

use DateTimeInterface;

interface TimeProvider
{
    public function getCurrentTime(): DateTimeInterface;

    public function getCurrentMicrotime(): float;
}
