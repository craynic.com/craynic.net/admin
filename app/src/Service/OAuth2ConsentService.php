<?php

namespace App\Service;

use App\EventSubscriber\AuthorizationRequestResolveSubscriber;
use App\ValueObject\OAuth2Scope;
use League\Bundle\OAuth2ServerBundle\Manager\ClientManagerInterface;
use League\OAuth2\Server\AuthorizationServer;
use League\OAuth2\Server\Exception\OAuthServerException;
use Symfony\Bridge\PsrHttpMessage\HttpMessageFactoryInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Twig\Environment;

final readonly class OAuth2ConsentService
{
    public function __construct(
        private Security $security,
        private ClientManagerInterface $clientManager,
        private Environment $twig,
        private UrlGeneratorInterface $router,
        private HttpMessageFactoryInterface $messageFactory,
        private AuthorizationServer $authorizationServer,
    ) {
    }

    public function renderConsentPage(Request $request): Response
    {
        $this->validateRequest($request);

        return new Response(
            $this->twig->render('oauth2/consent.html.twig', [
                'username' => $this->security->getToken()?->getUser()?->getUserIdentifier(),
                'client' => $this->clientManager->find((string) $request->query->get('client_id')),
                'scope' => OAuth2Scope::from($request->query->get('scope'))->getDescription(),
                'target_uri' => $this->router->generate('oauth2-consent-form', $request->query->all()),
            ])
        );
    }

    /**
     * @throws OAuthServerException
     */
    public function processConsent(Request $request, bool $wasApproved): Response
    {
        $this->validateRequest($request);

        $request->getSession()->set(
            AuthorizationRequestResolveSubscriber::SESSION_AUTHORIZATION_RESULT,
            $wasApproved
        );

        return new RedirectResponse(
            $this->router->generate('oauth2-authorize', $request->query->all())
        );
    }

    /**
     * @throws OAuthServerException
     */
    private function validateRequest(Request $request): void
    {
        $serverRequest = $this->messageFactory->createRequest($request);

        $this->authorizationServer->validateAuthorizationRequest($serverRequest);
    }
}
