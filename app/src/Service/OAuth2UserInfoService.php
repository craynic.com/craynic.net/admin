<?php

declare(strict_types=1);

namespace App\Service;

use JetBrains\PhpStorm\ArrayShape;
use League\Bundle\OAuth2ServerBundle\Security\Authentication\Token\OAuth2Token;
use League\OAuth2\Server\Exception\OAuthServerException;
use League\OAuth2\Server\ResourceServer;
use Nyholm\Psr7\ServerRequest;
use Symfony\Component\HttpFoundation\Request;

final readonly class OAuth2UserInfoService
{
    public function __construct(private ResourceServer $resourceServer)
    {
    }

    /**
     * @throws OAuthServerException
     */
    #[ArrayShape(['username' => "string"])]
    public function getUserInfoDataFromAccessToken(string $accessToken): array
    {
        /** @noinspection AllyPlainPhpInspection */
        $serverRequest = (new ServerRequest(Request::METHOD_GET, ''))
            ->withHeader('Authorization', sprintf('Bearer %s', $accessToken));

        $ret = $this->resourceServer->validateAuthenticatedRequest($serverRequest);

        return $this->toArray(
            userId: $ret->getAttribute('oauth_user_id', ''),
        );
    }

    #[ArrayShape(['username' => "string"])]
    public function getUserInfoDataFromOAuth2Token(OAuth2Token $token): array
    {
        return $this->toArray(
            userId: $token->getUserIdentifier(),
        );
    }

    #[ArrayShape(['username' => "string"])]
    private function toArray(string $userId): array
    {
        return [
            'username' => $userId,
        ];
    }
}
