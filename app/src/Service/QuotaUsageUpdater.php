<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\ReadModel\DovecotQuotaChangelog;
use App\EntityManager\ReadModelsEntityManager;
use App\Exception\QuotaNotFound;
use App\Repository\QuotaRepository;
use App\ValueObject\EmailAddress;
use App\ValueObject\QuotaUsageUpdateResult;
use Doctrine\ODM\MongoDB\DocumentManager;
use Psr\Log\LoggerInterface;

final readonly class QuotaUsageUpdater
{
    public function __construct(
        private ReadModelsEntityManager $entityManager,
        private DocumentManager $documentManager,
        private LoggerInterface $logger,
        private QuotaRepository $quotaRepository,
    ) {
    }

    public function update(): void
    {
        $ret = $this->updateOnce();

        while ($ret->notDeleted > 0) {
            $this->logger->debug(<<<'EOF'
Some of the records could not be deleted due to a parallel change, updating again...
EOF);

            $ret = $this->updateOnce();
        }
    }

    private function updateOnce(): QuotaUsageUpdateResult
    {
        $quotaChangelogRepo = $this->entityManager->getRepository(DovecotQuotaChangelog::class);
        /** @var DovecotQuotaChangelog[] $allQuotaChangelogs */
        $allQuotaChangelogs = $quotaChangelogRepo->findAll();

        if (empty($allQuotaChangelogs)) {
            $this->logger->debug('The quotas changelog is empty, nothing to do.');
            return new QuotaUsageUpdateResult(0, 0);
        }

        $this->logger->info('Updating quotas...');

        foreach ($allQuotaChangelogs as $quotaChangelog) {
            $emailAddress = new EmailAddress($quotaChangelog->username);

            $this->logger->debug("Updating quota for e-mail address $emailAddress...");

            try {
                $this->quotaRepository
                    ->findOneByDomain($emailAddress->domain)
                    ->updateUsageForEmailAddress($emailAddress, $quotaChangelog);
            } catch (QuotaNotFound) {
                continue;
            }
        }

        $this->documentManager->flush();
        $this->documentManager->clear();

        $deletedCount = 0;
        foreach ($allQuotaChangelogs as $quotaChangelog) {
            /** @noinspection SqlResolve */
            $query = $this->entityManager->createQuery(sprintf(
                'DELETE FROM %s q WHERE q.username=:username AND q.version=:version',
                DovecotQuotaChangelog::class,
            ));
            $deletedCount += $query->execute([
                'username' => $quotaChangelog->username,
                'version' => $quotaChangelog->version
            ]);
        }

        $this->logger->info(
            match (count($allQuotaChangelogs)) {
                1 => sprintf('Updating quotas finished, changed %d mailbox.', count($allQuotaChangelogs)),
                default => sprintf('Updating quotas finished, changed %d mailboxes.', count($allQuotaChangelogs))
            }
        );

        $this->entityManager->clear();

        return new QuotaUsageUpdateResult(count($allQuotaChangelogs), count($allQuotaChangelogs) - $deletedCount);
    }
}
