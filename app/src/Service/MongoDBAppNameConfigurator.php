<?php

/** @noinspection PhpUnused */

declare(strict_types=1);

namespace App\Service;

use Symfony\Component\DependencyInjection\EnvVarLoaderInterface;

final class MongoDBAppNameConfigurator implements EnvVarLoaderInterface
{
    private const string APPNAME_PARAMETER_NAME = 'appName';

    public function loadEnvVars(): array
    {
        if (empty($_ENV['APP_MONGO_CONN_URI'])) {
            return [];
        }

        return [
            'APP_MONGO_CONN_URI_WITH_APPNAME' => sprintf(
                '%1$s%2$s',
                $_ENV['APP_MONGO_CONN_URI'],
                empty($_ENV['APP_MONGO_APPNAME'])
                    ? ''
                    : sprintf(
                    '%1$s%2$s=%3$s',
                    str_contains($_ENV['APP_MONGO_CONN_URI'], '?') ? '&' : '?',
                    self::APPNAME_PARAMETER_NAME,
                    urlencode($_ENV['APP_MONGO_APPNAME'])
                )
            )
        ];
    }
}
