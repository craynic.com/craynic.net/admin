<?php

declare(strict_types=1);

namespace App\Service\TwoFactorAuth;

use App\Security\MailboxUser;
use Doctrine\ODM\MongoDB\DocumentManager;
use RuntimeException;
use Scheb\TwoFactorBundle\Model\PersisterInterface;

final readonly class Persister implements PersisterInterface
{
    public function __construct(private DocumentManager $documentManager)
    {
    }

    public function persist(object $user): void
    {
        if (!$user instanceof MailboxUser) {
            throw new RuntimeException('Invalid type of user to persist.');
        }

        $this->documentManager->flush();
    }
}
