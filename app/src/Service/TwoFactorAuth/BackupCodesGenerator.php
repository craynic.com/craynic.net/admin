<?php

declare(strict_types=1);

namespace App\Service\TwoFactorAuth;

use App\Repository\MailMailboxRepository;
use App\Security\MailboxUser;
use Random\Engine\Secure;
use Random\Randomizer;
use Symfony\Component\PasswordHasher\Hasher\PasswordHasherFactoryInterface;
use Symfony\Component\PasswordHasher\PasswordHasherInterface;

final readonly class BackupCodesGenerator
{
    private const int DEFAULT_BACKUP_CODES_COUNT = 10;
    private const int DEFAULT_BACKUP_CODES_LENGTH = 20;
    private const int DEFAULT_BACKUP_CODES_GROUP_LENGTH = 5;

    private PasswordHasherInterface $hasher;

    public function __construct(
        PasswordHasherFactoryInterface $passwordHasherFactory,
        private MailMailboxRepository $mailboxRepository
    ) {
        $this->hasher = $passwordHasherFactory->getPasswordHasher(MailboxUser::class);
    }

    /**
     * @return string[]
     */
    public function generateForUser(
        MailboxUser $user,
        int $count = self::DEFAULT_BACKUP_CODES_COUNT,
        int $length = self::DEFAULT_BACKUP_CODES_LENGTH,
        int $groupLength = self::DEFAULT_BACKUP_CODES_GROUP_LENGTH
    ): array {
        $rawBackupCodes = $this->generateRawCodes($count, $length, $groupLength);
        $hashedBackupCodes = $this->hashBackupCodes(...$rawBackupCodes);

        $this->mailboxRepository
            ->findByMailboxUser($user)
            ->getTwoFactorAuth()->setBackupCodes(...$hashedBackupCodes);

        return $rawBackupCodes;
    }

    /**
     * @return string[]
     */
    private function generateRawCodes(int $count, int $length, int $groupLength): array
    {
        $randomizer = new Randomizer(new Secure());

        $out = [];
        for ($i = 1; $i <= $count; $i++) {
            $out[] = implode('-', str_split($randomizer->getBytesFromString('0123456789', $length), $groupLength));
        }

        return $out;
    }

    /**
     * @return string[]
     */
    private function hashBackupCodes(string ...$rawBackupCodes): array
    {
        return array_combine(
            $rawBackupCodes,
            array_map(
                fn (int|string $rawCode): string => $this->hasher->hash((string) $rawCode),
                $rawBackupCodes
            )
        );
    }
}
