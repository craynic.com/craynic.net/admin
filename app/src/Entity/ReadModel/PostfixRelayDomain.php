<?php

declare(strict_types=1);

namespace App\Entity\ReadModel;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;

#[Entity]
#[Table(name: 'postfix_relay_domains')]
class PostfixRelayDomain
{
    #[Column(type: "string")]
    #[Id]
    public string $domain;

    public function __construct(string $domain)
    {
        $this->domain = $domain;
    }
}
