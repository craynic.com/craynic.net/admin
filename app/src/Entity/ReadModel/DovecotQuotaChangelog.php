<?php

declare(strict_types=1);

namespace App\Entity\ReadModel;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\Version;

#[Entity]
#[Table(name: 'dovecot_quota_changelog')]
class DovecotQuotaChangelog
{
    #[Column(type: "string")]
    #[Id]
    public readonly string $username;

    #[Column(type: "bigint", nullable: true, options: ['default' => null])]
    public ?int $bytes;

    #[Column(type: "integer", nullable: true, options: ['default' => null])]
    public ?int $messages;

    #[Version]
    #[Column(type: "integer")]
    public int $version;
}
