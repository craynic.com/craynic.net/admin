<?php

declare(strict_types=1);

namespace App\Entity\ReadModel;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;

#[Entity]
#[Table(name: 'dovecot_junk_settings')]
class DovecotJunkSettings
{
    #[Column(type: "string")]
    #[Id]
    public readonly string $username;

    #[Column(name: "junk_folder_name", type: "text", length: 100)]
    public string $junkFolderName;

    public function __construct(string $username)
    {
        $this->username = $username;
    }
}
