<?php

declare(strict_types=1);

namespace App\Entity\ReadModel;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;

#[Entity]
#[Table(name: 'postfix_sender_login_maps')]
class PostfixSenderLogin
{
    #[Column(type: "string")]
    #[Id]
    public string $user;

    #[Column(type: "string")]
    #[Id]
    public string $sender;

    public function __construct(string $user, string $sender)
    {
        $this->user = $user;
        $this->sender = $sender;
    }
}
