<?php

declare(strict_types=1);

namespace App\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

#[MongoDB\EmbeddedDocument]
class MailDkim
{
    #[MongoDB\Field(type: "bool")]
    private bool $isEnabled;

    public function __construct(
        #[MongoDB\Field(type: "string")] private readonly string $key,
        #[MongoDB\Field(type: "string")] private readonly string $selector
    ) {
        $this->isEnabled = false;
    }

    public function getKey(): string
    {
        return $this->key;
    }

    public function getSelector(): string
    {
        return $this->selector;
    }

    public function isEnabled(): bool
    {
        return $this->isEnabled;
    }

    public function enable(): void
    {
        $this->isEnabled = true;
    }

    public function disable(): void
    {
        $this->isEnabled = false;
    }
}
