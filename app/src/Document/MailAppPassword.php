<?php

declare(strict_types=1);

namespace App\Document;

use DateTimeInterface;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

#[MongoDB\EmbeddedDocument]
class MailAppPassword
{
    #[MongoDB\Field(type: "string")]
    private string $password;

    #[MongoDB\Field(type: "string")]
    private string $description;

    #[MongoDB\Field(type: "date")]
    private ?DateTimeInterface $lastUsed = null;

    public function __construct(string $password, string $description)
    {
        $this->password = $password;
        $this->description = $description;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getLastUsed(): ?DateTimeInterface
    {
        return $this->lastUsed;
    }

    public function setLastUsed(?DateTimeInterface $lastUsed): void
    {
        $this->lastUsed = $lastUsed;
    }
}
