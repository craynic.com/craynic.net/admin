<?php

declare(strict_types=1);

namespace App\Document;

use App\Exception\MailTargetAlreadyExists;
use App\Exception\MailTargetNotFound;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Doctrine\ODM\MongoDB\Mapping\ClassMetadata;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

#[MongoDB\EmbeddedDocument]
class MailRedirect implements MailTargetAggregate
{
    /** @noinspection PhpPropertyOnlyWrittenInspection */
    #[MongoDB\Field(type: "string")]
    private string $id;

    /** @noinspection PhpPropertyOnlyWrittenInspection */
    #[MongoDB\Field(type: "string")]
    private string $mailbox;

    #[MongoDB\Field(type: "string")]
    private ?string $description = null;

    /** @var Collection<int, MailTarget> */
    #[MongoDB\EmbedMany(
        strategy: ClassMetadata::STORAGE_STRATEGY_SET_ARRAY,
        targetDocument: MailTarget::class,
        collectionClass: ''
    )]
    private Collection $targets;

    #[MongoDB\Field(type: "bool")]
    private bool $isActive;

    public function __construct()
    {
        $this->targets = new ArrayCollection();
    }

    public function getId(): UuidInterface
    {
        return Uuid::fromBytes($this->id);
    }

    public function getMailbox(): string
    {
        return $this->mailbox;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @return Collection<int, MailTarget>
     */
    public function getTargets(): Collection
    {
        return $this->targets;
    }

    private function getTargetByTargetAddress(string $targetAddress): MailTarget
    {
        foreach ($this->getTargets() as $target) {
            if (strcasecmp($target->getTargetAddress(), $targetAddress) === 0) {
                return $target;
            }
        }

        throw new MailTargetNotFound($targetAddress);
    }

    public function addTargets(string ...$targetAddresses): void
    {
        foreach ($targetAddresses as $targetAddress) {
            try {
                $this->getTargetByTargetAddress($targetAddress);

                throw new MailTargetAlreadyExists($targetAddress);
            } catch (MailTargetNotFound) {
            }

            $this->getTargets()->add(new MailTarget($targetAddress));
        }
    }

    public function removeTargets(string ...$targetAddresses): void
    {
        foreach ($targetAddresses as $targetAddress) {
            $this->getTargets()->removeElement($this->getTargetByTargetAddress($targetAddress));
        }
    }

    public function setTargets(string ...$targetAddresses): void
    {
        $this->getTargets()->clear();
        $this->addTargets(...$targetAddresses);
    }

    public function isActive(): bool
    {
        return $this->isActive;
    }

    public function setActive(bool $isActive): void
    {
        $this->isActive = $isActive;
    }

    public function activateTargets(string ...$targetAddresses): void
    {
        foreach ($targetAddresses as $targetAddress) {
            $this->getTargetByTargetAddress($targetAddress)->activate();
        }
    }

    public function deactivateTargets(string ...$targetAddresses): void
    {
        foreach ($targetAddresses as $targetAddress) {
            $this->getTargetByTargetAddress($targetAddress)->deactivate();
        }
    }

    public function toArray(): array
    {
        return [
            'id' => $this->getId()->toString(),
            'mailbox' => $this->getMailbox(),
            'description' => $this->getDescription(),
            'targets' => $this->getTargets()->map(
                fn (MailTarget $target): array => $target->toArray(),
            )->toArray(),
            'isActive' => $this->isActive(),
        ];
    }
}
