<?php

/**
 * @noinspection PhpUnusedFieldDefaultValueInspection
 *
 * The QuotaUsageOptionalMax::$max property must have a default value - Doctrine does not use the constructor
 * to hydrate objects; not having a default value for the property would end up in fatal error
 */

declare(strict_types=1);

namespace App\Document;

use App\Exception\UnlimitedQuotaMax;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

#[MongoDB\EmbeddedDocument]
class QuotaUsageOptionalMax extends QuotaUsageAbstract
{
    #[MongoDB\Field(type: "int")] private ?int $max = null;

    public function __construct(int $current, ?int $max = null)
    {
        parent::__construct($current);

        $this->max = $max;
    }

    public function getMax(): int
    {
        if ($this->max === null) {
            throw new UnlimitedQuotaMax();
        }

        return $this->max;
    }
}
