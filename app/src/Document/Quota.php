<?php

/** @noinspection PhpUnusedPrivateFieldInspection */
/** @noinspection PhpPropertyOnlyWrittenInspection */

declare(strict_types=1);

namespace App\Document;

use App\Entity\ReadModel\DovecotQuotaChangelog;
use App\Exception\QuotaDomainNotFound;
use App\Exception\QuotaMailboxNotFound;
use App\Exception\UnlimitedDomainQuota;
use App\Exception\UnlimitedMailboxQuota;
use App\Repository\QuotaRepository;
use App\ValueObject\EmailAddress;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Doctrine\ODM\MongoDB\Mapping\ClassMetadata;
use MongoDB\BSON\ObjectId;
use MongoDB\BSON\ObjectIdInterface;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

#[MongoDB\Document(collection: "quota", repositoryClass: QuotaRepository::class)]
class Quota
{
    #[MongoDB\Id]
    private string $_id;

    #[MongoDB\Field(type: "string")]
    private string $id;

    #[MongoDB\Field(type: "int")]
    #[MongoDB\Version]
    private int $version;

    /** @var Collection<QuotaDomain> */
    #[MongoDB\EmbedMany(
        strategy: ClassMetadata::STORAGE_STRATEGY_SET_ARRAY,
        targetDocument: QuotaDomain::class
    )]
    private Collection $domains;

    #[MongoDB\EmbedOne(targetDocument: QuotaUsageMandatoryMax::class)]
    private QuotaUsageMandatoryMax $usage;

    #[MongoDB\EmbedOne(targetDocument: QuotaReporting::class)]
    private QuotaReporting $reporting;

    public function __construct()
    {
        $this->domains = new ArrayCollection();
    }

    public function getObjectId(): ObjectIdInterface
    {
        return new ObjectId($this->_id);
    }

    public function getId(): UuidInterface
    {
        return Uuid::fromBytes($this->id);
    }

    /**
     * @return Collection<QuotaDomain>
     */
    public function getDomains(): Collection
    {
        return $this->domains;
    }

    public function getUsage(): QuotaUsageMandatoryMax
    {
        return $this->usage;
    }

    public function getReporting(): QuotaReporting
    {
        return $this->reporting;
    }

    /**
     * @throws QuotaDomainNotFound
     */
    public function updateUsageForEmailAddress(EmailAddress $emailAddress, DovecotQuotaChangelog $changelog): void
    {
        $this
            ->getQuotaDomainByName($emailAddress->domain)
            ->updateUsageForMailbox($emailAddress->mailbox, $changelog);

        $this->getUsage()->setCurrent(
            array_sum(array_map(
                fn (QuotaDomain $domain): int => $domain->getUsage()->getCurrent(),
                $this->getDomains()->toArray()
            ))
        );
    }

    /**
     * @throws QuotaDomainNotFound
     */
    public function getQuotaAllocationForMailbox(EmailAddress $mailboxEmailAddress): int
    {
        $quotaDomain = $this->getQuotaDomainByName($mailboxEmailAddress->domain);

        try {
            $quotaMailbox = $quotaDomain->getQuotaMailboxByName($mailboxEmailAddress->mailbox);
        } catch (QuotaMailboxNotFound) {
            try {
                return $quotaDomain->getFreeSpace();
            } catch (UnlimitedDomainQuota) {
                return $this->getFreeSpace();
            }
        }

        try {
            return $quotaMailbox->getMaximumUsage();
        } catch (UnlimitedMailboxQuota) {
            try {
                return $quotaDomain->getFreeSpace() + $quotaMailbox->getAllocatedSpace();
            } catch (UnlimitedDomainQuota) {
                return $this->getFreeSpace() + $quotaMailbox->getAllocatedSpace();
            }
        }
    }

    private function getFreeSpace(): int
    {
        return max(0, $this->getUsage()->getMax() - $this->getAllocatedSpaceForDomains());
    }

    private function getAllocatedSpaceForDomains(): int
    {
        $allocatedSpace = 0;

        foreach ($this->getDomains() as $domain) {
            $allocatedSpace += $domain->getAllocatedSpace();
        }

        return $allocatedSpace;
    }

    private function getQuotaDomainByName(string $domainToMatch): QuotaDomain
    {
        foreach ($this->getDomains() as $domain) {
            if (strcasecmp($domain->getDomain(), $domainToMatch) === 0) {
                return $domain;
            }
        }

        throw new QuotaDomainNotFound($domainToMatch);
    }

    /**
     * @return string[]
     */
    public function getAllDomainNames(): array
    {
        $domainNames = [];
        foreach ($this->getDomains() as $domain) {
            $domainNames[] = $domain->getDomain();
        }

        return $domainNames;
    }
}
