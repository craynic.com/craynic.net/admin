<?php

declare(strict_types=1);

namespace App\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

#[MongoDB\EmbeddedDocument]
class MailMailboxTwoFactorAuthGoogleAuth
{
    #[MongoDB\Field(type: "bool")]
    private bool $isEnabled;

    #[MongoDB\Field(type: "string")]
    private string $secret;

    public function __construct(bool $isEnabled, string $secret)
    {
        $this->isEnabled = $isEnabled;
        $this->secret = $secret;
    }

    public function isEnabled(): bool
    {
        return $this->isEnabled;
    }

    public function enable(): void
    {
        $this->isEnabled = true;
    }

    public function disable(): void
    {
        $this->isEnabled = false;
    }

    public function getSecret(): string
    {
        return $this->secret;
    }
}
