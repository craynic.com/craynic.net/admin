<?php

/** @noinspection PhpPropertyOnlyWrittenInspection */

declare(strict_types=1);

namespace App\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

#[MongoDB\EmbeddedDocument]
class MailBcc
{
    #[MongoDB\Field(type: "string")]
    private string $bcc;

    #[MongoDB\Field(type: "bool")]
    private bool $isActive;

    public function getBcc(): string
    {
        return $this->bcc;
    }

    public function isActive(): bool
    {
        return $this->isActive;
    }
}
