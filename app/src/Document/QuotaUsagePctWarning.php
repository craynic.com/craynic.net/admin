<?php

/** @noinspection PhpPropertyOnlyWrittenInspection */

declare(strict_types=1);

namespace App\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

#[MongoDB\EmbeddedDocument]
class QuotaUsagePctWarning
{
    /** @var int[] */
    #[MongoDB\Field(type: 'collection')]
    private array $levels;

    #[MongoDB\EmbedOne(targetDocument: QuotaUsagePctWarningLast::class)]
    private ?QuotaUsagePctWarningLast $lastWarning = null;

    #[MongoDB\EmbedOne(targetDocument: QuotaUsagePctWarningLast::class)]
    private ?QuotaUsagePctWarningLast $lastOk = null;

    public function triggerWarningIfNeeded(int $currentLevel): void
    {
        $reachedQuotaPcts = array_filter(
            $this->levels,
            fn (int $warningLevel): bool => $warningLevel <= $currentLevel
        );

        if (empty($reachedQuotaPcts)) {
            $this->triggerOk();
        } else {
            $this->triggerWarning(max($reachedQuotaPcts));
        }
    }

    private function triggerWarning(int $reachedLevel): void
    {
        if ($this->lastWarning === null || $reachedLevel > $this->lastWarning->getLevel()) {
            $this->lastOk = null;
        } elseif ($reachedLevel < $this->lastWarning->getLevel()) {
            $this->setLastOk($this->lastWarning->getLevel());
        }

        $this->setLastWarning($reachedLevel);
    }

    private function triggerOk(): void
    {
        if ($this->lastWarning === null) {
            return;
        }

        $this->setLastOk($this->lastWarning->getLevel());
        $this->lastWarning = null;
    }

    private function setLastWarning(int $reachedLevel): void
    {
        if ($this->lastWarning === null) {
            $this->lastWarning = new QuotaUsagePctWarningLast($reachedLevel);
        } else {
            $this->lastWarning->setLevel($reachedLevel);
        }
    }

    private function setLastOk(int $reachedLevel): void
    {
        if ($this->lastOk === null) {
            $this->lastOk = new QuotaUsagePctWarningLast($reachedLevel);
        } else {
            $this->lastOk->setLevel($reachedLevel);
        }
    }
}
