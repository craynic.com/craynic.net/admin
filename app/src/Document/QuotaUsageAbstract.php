<?php

declare(strict_types=1);

namespace App\Document;

use App\Exception\UnlimitedQuotaMax;
use App\Exception\ZeroQuotaMax;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

abstract class QuotaUsageAbstract
{
    #[MongoDB\Field(type: "int")]
    protected int $current;

    #[MongoDB\EmbedOne(targetDocument: QuotaUsagePctWarning::class)]
    protected ?QuotaUsagePctWarning $pctWarning = null;

    public function __construct(int $current)
    {
        $this->current = $current;
    }

    public function getCurrent(): int
    {
        return $this->current;
    }

    public function setCurrent(int $current): void
    {
        $this->current = $current;

        $this->triggerWarningForGivenMaxIfNeeded();
    }

    abstract public function getMax(): int;

    public function getPercentage(): int
    {
        if ($this->getMax() === 0) {
            throw new ZeroQuotaMax();
        }

        return (int) ceil(100 * $this->getCurrent() / $this->getMax());
    }

    protected function triggerWarningForGivenMaxIfNeeded(): void
    {
        try {
            $this->pctWarning?->triggerWarningIfNeeded($this->getPercentage());
        } catch (UnlimitedQuotaMax | ZeroQuotaMax) {
        }
    }
}
