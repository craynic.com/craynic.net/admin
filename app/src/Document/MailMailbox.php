<?php

/** @noinspection PhpPropertyOnlyWrittenInspection */

declare(strict_types=1);

namespace App\Document;

use App\Exception\TwoFactorNotSet;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

#[MongoDB\EmbeddedDocument]
class MailMailbox
{
    #[MongoDB\Field(type: "string")]
    private string $id;

    #[MongoDB\Field(name: "mailbox", type: "string")]
    private string $name;

    #[MongoDB\Field(type: "string")]
    private ?string $description = null;

    #[MongoDB\Field(type: "string")]
    private ?string $password = null;

    #[MongoDB\Field(type: "bool")]
    private bool $isActive;

    #[MongoDB\Field(type: "bool")]
    private bool $isLoginEnabled;

    #[MongoDB\Field(type: "bool")]
    private bool $isDeliveryEnabled;

    #[MongoDB\Field(type: "bool")]
    private bool $isCatchAll;

    /** @var Collection<MailExtraSender> */
    #[MongoDB\EmbedMany(targetDocument: MailExtraSender::class, collectionClass: '')]
    private Collection $extraSenders;

    #[MongoDB\EmbedOne(nullable: true, targetDocument: MailBcc::class)]
    private ?MailBcc $bcc = null;

    /** @var Collection<MailAppPassword> */
    #[MongoDB\EmbedMany(targetDocument: MailAppPassword::class, collectionClass: '')]
    private Collection $appPasswords;

    #[MongoDB\Field(type: "date")]
    private ?DateTimeInterface $lastLogin = null;

    #[MongoDB\EmbedOne(nullable: true, targetDocument: MailMailboxRedirect::class)]
    private ?MailMailboxRedirect $redirect = null;

    #[MongoDB\Field(type: "string")]
    private ?string $junkFolderName = null;

    #[MongoDB\EmbedOne(targetDocument: MailMailboxTwoFactorAuth::class)]
    private ?MailMailboxTwoFactorAuth $twoFactorAuth = null;

    public function __construct()
    {
        $this->extraSenders = new ArrayCollection();
        $this->appPasswords = new ArrayCollection();
    }

    public function getId(): UuidInterface
    {
        return Uuid::fromBytes($this->id);
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(?string $password): void
    {
        $this->password = $password;
    }

    public function isActive(): bool
    {
        return $this->isActive;
    }

    public function isLoginEnabled(): bool
    {
        return $this->isLoginEnabled;
    }

    public function isDeliveryEnabled(): bool
    {
        return $this->isDeliveryEnabled;
    }

    public function isCatchAll(): bool
    {
        return $this->isCatchAll;
    }

    /**
     * @return Collection<MailExtraSender>
     */
    public function getExtraSenders(): Collection
    {
        return $this->extraSenders;
    }

    public function getBcc(): ?MailBcc
    {
        return $this->bcc;
    }

    /**
     * @return Collection<MailAppPassword>
     */
    public function getAppPasswords(): Collection
    {
        return $this->appPasswords;
    }

    public function addAppPassword(MailAppPassword $password): void
    {
        $this->appPasswords->add($password);
    }

    public function setAppPassword(MailAppPassword $password): void
    {
        $this->appPasswords = new ArrayCollection([$password]);
    }

    public function getLastLogin(): ?DateTimeInterface
    {
        return $this->lastLogin;
    }

    public function setLastLogin(?DateTimeInterface $lastLogin): void
    {
        $this->lastLogin = $lastLogin;
    }

    public function getRedirect(): ?MailMailboxRedirect
    {
        return $this->redirect;
    }

    public function getJunkFolderName(): ?string
    {
        return $this->junkFolderName;
    }

    public function getTwoFactorAuth(): MailMailboxTwoFactorAuth
    {
        if (null === $this->twoFactorAuth) {
            throw new TwoFactorNotSet();
        }

        return $this->twoFactorAuth;
    }

    public function removeTwoFactorAuth(): void
    {
        if (null === $this->twoFactorAuth) {
            throw new TwoFactorNotSet();
        }

        $this->twoFactorAuth = null;
    }

    public function recreateTwoFactorAuth(): MailMailboxTwoFactorAuth
    {
        return $this->twoFactorAuth = new MailMailboxTwoFactorAuth();
    }
}
