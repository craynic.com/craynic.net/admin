<?php

/** @noinspection PhpPropertyOnlyWrittenInspection */

declare(strict_types=1);

namespace App\Document;

use DateTimeImmutable;
use DateTimeInterface;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

#[MongoDB\EmbeddedDocument]
class QuotaReporting
{
    #[MongoDB\Field(type: "string")]
    private string $frequency;

    #[MongoDB\Field(type: "date")]
    private ?DateTimeInterface $lastSent = null;

    public function getFrequency(): string
    {
        return $this->frequency;
    }

    public function getLastSent(): ?DateTimeInterface
    {
        return $this->lastSent;
    }

    public function shouldSendReport(DateTimeInterface $now): bool
    {
        $dateFormat = match($this->getFrequency()) {
            'daily' => 'Y-m-d',
            'weekly' => 'Y-W',
            'monthly' => 'Y-m',
        };

        return $this->getLastSent()?->format($dateFormat) !== $now->format($dateFormat);
    }

    public function markAsSent(DateTimeInterface $now): void
    {
        $this->lastSent = DateTimeImmutable::createFromInterface($now);
    }
}
