<?php

/** @noinspection PhpUnusedPrivateFieldInspection */
/** @noinspection PhpPropertyOnlyWrittenInspection */

declare(strict_types=1);

namespace App\Document;

use App\Repository\MailAdminRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Doctrine\ODM\MongoDB\Mapping\ClassMetadata;

#[MongoDB\Document(collection: "admins", repositoryClass: MailAdminRepository::class)]
class MailAdmin
{
    #[MongoDB\Id]
    private string $_id;

    #[MongoDB\Field(type: "string")]
    private string $id;

    #[MongoDB\Field(type: "int")]
    #[MongoDB\Version]
    private int $version;

    #[MongoDB\Field(type: "string")]
    private string $email;

    #[MongoDB\Field(type: "bool")]
    private ?bool $isSuperAdmin = false;

    /** @var Collection<MailDomain> */
    #[MongoDB\ReferenceMany(
        storeAs: ClassMetadata::REFERENCE_STORE_AS_ID,
        targetDocument: MailDomain::class
    )]
    public Collection $domains;

    public function __construct()
    {
        $this->domains = new ArrayCollection();
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function isSuperAdmin(): bool
    {
        return $this->isSuperAdmin ?? false;
    }

    public function canManageDomain(string $domainName): bool
    {
        if ($this->isSuperAdmin()) {
            return true;
        }

        foreach ($this->domains as $mailDomain) {
            if ($mailDomain->getName() === $domainName) {
                return true;
            }
        }

        return false;
    }
}
