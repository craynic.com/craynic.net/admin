<?php

/** @noinspection PhpPropertyOnlyWrittenInspection */

declare(strict_types=1);

namespace App\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

#[MongoDB\EmbeddedDocument]
class QuotaUsagePctWarningLast
{
    #[MongoDB\Field(type: 'bool')]
    private bool $notificationSent = false;

    public function __construct(#[MongoDB\Field(type: 'int')] private int $level)
    {
    }

    public function getLevel(): int
    {
        return $this->level;
    }

    public function setLevel(int $level): void
    {
        if ($this->level !== $level) {
            $this->notificationSent = false;
        }

        $this->level = $level;
    }
}
