<?php

/** @noinspection PhpPropertyOnlyWrittenInspection */

declare(strict_types=1);

namespace App\Document;

use App\Entity\ReadModel\DovecotQuotaChangelog;
use App\Exception\QuotaMailboxNotFound;
use App\Exception\UnlimitedDomainQuota;
use App\Exception\UnlimitedQuotaMax;
use App\ValueObject\EmailAddress;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Doctrine\ODM\MongoDB\Mapping\ClassMetadata;

#[MongoDB\EmbeddedDocument]
class QuotaDomain
{
    #[MongoDB\Field(type: "string")]
    private string $domain;

    /** @var Collection<QuotaMailbox> */
    #[MongoDB\EmbedMany(
        strategy: ClassMetadata::STORAGE_STRATEGY_SET_ARRAY,
        targetDocument: QuotaMailbox::class,
        collectionClass: ''
    )]
    private Collection $mailboxes;

    #[MongoDB\EmbedOne(targetDocument: QuotaUsageOptionalMax::class)]
    private QuotaUsageOptionalMax $usage;

    public function __construct()
    {
        $this->mailboxes = new ArrayCollection();
    }

    public function getDomain(): string
    {
        return $this->domain;
    }

    /**
     * @return Collection<QuotaMailbox>
     */
    public function getMailboxes(): Collection
    {
        return $this->mailboxes;
    }

    public function getUsage(): QuotaUsageOptionalMax
    {
        return $this->usage;
    }

    public function updateUsageForMailbox(string $mailbox, DovecotQuotaChangelog $changelog): void
    {
        if ($changelog->bytes === null) {
            try {
                $this->deleteQuotaMailboxByName($mailbox);
            } catch (QuotaMailboxNotFound) {
            }
        } else {
            try {
                $this->getQuotaMailboxByName($mailbox)->getUsage()->setCurrent($changelog->bytes);
            } catch (QuotaMailboxNotFound) {
                $this->getMailboxes()->add(new QuotaMailbox($mailbox, $changelog->bytes));
            }
        }

        $this->getUsage()->setCurrent(
            array_sum(array_map(
                fn (QuotaMailbox $mailbox): int => $mailbox->getUsage()->getCurrent(),
                $this->getMailboxes()->toArray()
            ))
        );
    }

    public function getMaximumUsage(): int
    {
        try {
            return $this->getUsage()->getMax();
        } catch (UnlimitedQuotaMax) {
            throw new UnlimitedDomainQuota();
        }
    }

    public function getFreeSpace(): int
    {
        return max(0, $this->getMaximumUsage() - $this->getAllocatedSpaceForMailboxes());
    }

    public function getAllocatedSpace(): int
    {
        try {
            return $this->getMaximumUsage();
        } catch (UnlimitedDomainQuota) {
            return $this->getAllocatedSpaceForMailboxes();
        }
    }

    public function getAllocatedSpaceForMailboxes(): int
    {
        $allocatedSpace = 0;
        foreach ($this->getMailboxes() as $mailbox) {
            $allocatedSpace += $mailbox->getAllocatedSpace();
        }

        return $allocatedSpace;
    }

    public function getQuotaMailboxByName(string $mailboxToMatch): QuotaMailbox
    {
        foreach ($this->getMailboxes() as $mailbox) {
            if (strcasecmp($mailbox->getMailbox(), $mailboxToMatch) === 0) {
                return $mailbox;
            }
        }

        throw new QuotaMailboxNotFound(EmailAddress::fromValues($this->getDomain(), $mailboxToMatch));
    }

    public function deleteQuotaMailboxByName(string $mailboxToMatch): void
    {
        $this->getMailboxes()->removeElement(
            $this->getQuotaMailboxByName($mailboxToMatch)
        );
    }
}
