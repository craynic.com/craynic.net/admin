<?php

declare(strict_types=1);

namespace App\Document;

use Doctrine\Common\Collections\Collection;

interface MailTargetAggregate
{
    public function isActive(): bool;

    /**
     * @return Collection<int, MailTarget>
     */
    public function getTargets(): Collection;
}
