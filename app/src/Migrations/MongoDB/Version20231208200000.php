<?php

declare(strict_types=1);

namespace App\Migrations\MongoDB;

use AntiMattr\MongoDB\Migrations\AbstractMigration;
use App\Migrations\MongoDB\Traits\Utils;
use MongoDB\Database;

class Version20231208200000 extends AbstractMigration
{
    use Utils;

    private static string $domainsCollectionName = 'domains';

    public function getDescription(): string
    {
        return 'Applies new version of the domains schema (support for 2FA backup codes)';
    }

    public function up(Database $db): void
    {
        $this->applySchema($db, static::$domainsCollectionName, 'src/Resources/domains.schema.v6.json');
    }

    public function down(Database $db): void
    {
        $this->applySchema($db, static::$domainsCollectionName, 'src/Resources/domains.schema.v5.json');

        $collection = $db->{static::$domainsCollectionName};
        $collection->updateMany([], ['$unset' => ['mailboxes.$[].twoFactorAuth.backupCodes' => true]]);
    }
}
