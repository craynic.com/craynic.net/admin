<?php

declare(strict_types=1);

namespace App\Migrations\MongoDB;

use AntiMattr\MongoDB\Migrations\AbstractMigration;
use App\Migrations\MongoDB\Traits\Utils;
use MongoDB\Database;

class Version20220620221400 extends AbstractMigration
{
    use Utils;

    private static string $domainsCollectionName = 'domains';

    public function getDescription(): string
    {
        return 'Applies new version of the domains schema (drop greylisting, add mailbox redirect)';
    }

    public function up(Database $db): void
    {
        $this->applySchema($db, static::$domainsCollectionName, 'src/Resources/domains.schema.v2.json');

        $collection = $db->{static::$domainsCollectionName};
        $collection->updateMany([], [
            '$unset' => [
                'isGreylistingEnabled' => true,
                'aliases.$[].isGreylistingEnabled' => true,
                'mailboxes.$[].isGreylistingEnabled' => true,
                'redirects.$[].isGreylistingEnabled' => true,
            ]
        ]);
    }

    public function down(Database $db): void
    {
        $this->applySchema($db, static::$domainsCollectionName, 'src/Resources/domains.schema.v1.json');

        $collection = $db->{static::$domainsCollectionName};

        $collection->updateMany([], [
            '$set' => [
                'isGreylistingEnabled' => true,
                'aliases.$[].isGreylistingEnabled' => true,
                'mailboxes.$[].isGreylistingEnabled' => true,
                'redirects.$[].isGreylistingEnabled' => true,
            ],
            '$unset' => [
                'mailboxes.$[].redirect' => true,
            ]
        ]);
    }
}
