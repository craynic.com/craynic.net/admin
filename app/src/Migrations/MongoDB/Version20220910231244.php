<?php

declare(strict_types=1);

namespace App\Migrations\MongoDB;

use AntiMattr\MongoDB\Migrations\AbstractMigration;
use App\Migrations\MongoDB\Traits\Utils;
use MongoDB\Database;

class Version20220910231244 extends AbstractMigration
{
    use Utils;

    private static string $domainsCollectionName = 'domains';

    public function getDescription(): string
    {
        return 'Applies new version of the domains schema (drops quota related info from domains collection)';
    }

    public function up(Database $db): void
    {
        $this->applySchema($db, static::$domainsCollectionName, 'src/Resources/domains.schema.v4.json');

        $collection = $db->{static::$domainsCollectionName};
        $collection->updateMany([], ['$unset' => ['quota' => true, 'mailboxes.$[].quota' => true]]);
    }

    public function down(Database $db): void
    {
        $this->applySchema($db, static::$domainsCollectionName, 'src/Resources/domains.schema.v3.json');

        $collection = $db->{static::$domainsCollectionName};
        $collection->updateMany([], ['$set' => [
            'quota' => ['current' => 0, 'max' => 0],
            'mailboxes.$[].quota' => ['current' => 0]
        ]]);
    }
}
