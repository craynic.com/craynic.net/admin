<?php

declare(strict_types=1);

namespace App\Migrations\MongoDB;

use AntiMattr\MongoDB\Migrations\AbstractMigration;
use App\Migrations\MongoDB\Traits\Utils;
use MongoDB\Database;

class Version20220324202340 extends AbstractMigration
{
    use Utils;

    private static string $domainsCollectionName = 'domains';

    public function getDescription(): string
    {
        return 'Creates the indexed domain collection with JSON schema validation';
    }

    public function up(Database $db): void
    {
        $this->createCollection($db, static::$domainsCollectionName, 'src/Resources/domains.schema.v1.json');

        $collection = $db->{static::$domainsCollectionName};

        $collection->createIndex(['id' => 1], ['unique' => true]);
        $collection->createIndex(['domain' => 1], ['unique' => true]);
    }

    public function down(Database $db): void
    {
        $db->dropCollection(static::$domainsCollectionName);
    }
}
