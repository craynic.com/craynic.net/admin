<?php

declare(strict_types=1);

namespace App\Migrations\MongoDB;

use AntiMattr\MongoDB\Migrations\AbstractMigration;
use App\Migrations\MongoDB\Traits\Utils;
use MongoDB\Database;

class Version20230216144500 extends AbstractMigration
{
    use Utils;

    private static string $adminsCollectionName = 'admins';
    private static string $quotaCollectionName = 'quota';

    public function getDescription(): string
    {
        return 'Creates the admins collection';
    }

    public function up(Database $db): void
    {
        $this->createCollection($db, static::$adminsCollectionName, 'src/Resources/admins.schema.v1.json');

        $adminsCollection = $db->{static::$adminsCollectionName};

        $adminsCollection->createIndex(['id' => 1], ['unique' => true]);
        $adminsCollection->createIndex(['username' => 1], ['unique' => true]);
        $adminsCollection->createIndex(['domains' => 1]);
        $adminsCollection->createIndex(['isSuperAdmin' => 1]);

        $this->applySchema($db, static::$quotaCollectionName, 'src/Resources/quota.schema.v4.json');

        $quotaCollection = $db->{static::$quotaCollectionName};
        $quotaCollection->updateMany([], ['$set' => ['reporting' => ['frequency' => 'weekly']]]);
    }

    public function down(Database $db): void
    {
        $db->dropCollection(static::$adminsCollectionName);

        $this->applySchema($db, static::$quotaCollectionName, 'src/Resources/quota.schema.v3.json');

        $quotaCollection = $db->{static::$quotaCollectionName};
        $quotaCollection->updateMany([], ['$unset' => ['reporting' => true]]);
    }
}
