<?php

declare(strict_types=1);

namespace App\Migrations\MongoDB;

use AntiMattr\MongoDB\Migrations\AbstractMigration;
use App\Migrations\MongoDB\Traits\Utils;
use MongoDB\Database;

class Version20240223220000 extends AbstractMigration
{
    use Utils;

    private static string $adminsCollectionName = 'admins';

    public function getDescription(): string
    {
        return 'Drops username/password fields from the admin collection';
    }

    public function up(Database $db): void
    {
        $this->applySchema($db, static::$adminsCollectionName, 'src/Resources/admins.schema.v2.json');

        $adminsCollection = $db->{static::$adminsCollectionName};

        $adminsCollection->createIndex(['email' => 1], ['unique' => true]);
        $adminsCollection->dropIndex('username_1');

        $adminsCollection->updateMany([], ['$unset' => ['username' => true, 'password' => true]]);
    }

    public function down(Database $db): void
    {
        $this->applySchema($db, static::$adminsCollectionName, 'src/Resources/admins.schema.v1.json');

        $adminsCollection = $db->{static::$adminsCollectionName};
        $adminsCollection->updateMany([], [['$set' => ['username' => '$email']]]);

        $adminsCollection->createIndex(['username' => 1], ['unique' => true]);
        $adminsCollection->dropIndex('email_1');
    }
}
