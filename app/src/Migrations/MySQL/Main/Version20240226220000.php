<?php

declare(strict_types=1);

namespace App\Migrations\MySQL\Main;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20240226220000 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Increase data size for session';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE `sessions` CHANGE `sess_data` `sess_data` MEDIUMBLOB NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE `sessions` CHANGE `sess_data` `sess_data` BLOB NOT NULL');
    }

    public function isTransactional(): bool
    {
        return false;
    }
}
