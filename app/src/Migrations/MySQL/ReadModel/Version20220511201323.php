<?php

declare(strict_types=1);

namespace App\Migrations\MySQL\ReadModel;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220511201323 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql(<<<'SQL'
CREATE TABLE opendkim_keytable (
    domain VARCHAR(255) NOT NULL,
    dkim_selector VARCHAR(255) NOT NULL,
    dkim_key MEDIUMTEXT NOT NULL,
    PRIMARY KEY(domain)
) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB
SQL);

        $this->addSql(<<<'SQL'
CREATE TABLE opendkim_signingtable (
    domain VARCHAR(255) NOT NULL,
    PRIMARY KEY(domain)
) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB
SQL);
    }

    public function down(Schema $schema): void
    {
        $this->addSql(/** @lang MySQL */'DROP TABLE opendkim_keytable');
        $this->addSql(/** @lang MySQL */'DROP TABLE opendkim_signingtable');
    }

    public function isTransactional(): bool
    {
        return false;
    }
}
