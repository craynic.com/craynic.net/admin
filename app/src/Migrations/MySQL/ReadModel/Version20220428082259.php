<?php

declare(strict_types=1);

namespace App\Migrations\MySQL\ReadModel;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220428082259 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql(<<<'SQL'
CREATE TABLE dovecot_quota (
    username VARCHAR(255) NOT NULL,
    bytes BIGINT DEFAULT 0 NOT NULL,
    messages INT DEFAULT 0 NOT NULL,
    PRIMARY KEY(username)
) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB
SQL);

        $this->addSql(<<<'SQL'
CREATE TABLE dovecot_quota_changelog (
    username VARCHAR(255) NOT NULL,
    bytes BIGINT DEFAULT NULL,
    messages INT DEFAULT NULL,
    version INT DEFAULT 1 NOT NULL,
    PRIMARY KEY(username)
) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB
SQL);

        $this->addSql(<<<'SQL'
CREATE TRIGGER `dovecot_quota_insert` AFTER INSERT ON `dovecot_quota`
FOR EACH ROW
    INSERT INTO `dovecot_quota_changelog` (`username`, `bytes`, `messages`, `version`)
    VALUES(NEW.`username`, NEW.`bytes`, NEW.`messages`, 1)
    ON DUPLICATE KEY UPDATE `bytes`=NEW.`bytes`, `messages`=NEW.`messages`, `version`=`version`+1
SQL);

        $this->addSql(<<<'SQL'
CREATE TRIGGER `dovecot_quota_update` AFTER UPDATE ON `dovecot_quota`
    FOR EACH ROW
    INSERT INTO `dovecot_quota_changelog` (`username`, `bytes`, `messages`, `version`)
    VALUES(NEW.`username`, NEW.`bytes`, NEW.`messages`, 1)
    ON DUPLICATE KEY UPDATE `bytes`=NEW.`bytes`, `messages`=NEW.`messages`, `version`=`version`+1
SQL);

        $this->addSql(<<<'SQL'
CREATE TRIGGER `dovecot_quota_delete` AFTER DELETE ON `dovecot_quota`
    FOR EACH ROW
    INSERT INTO `dovecot_quota_changelog` (`username`, `bytes`, `messages`, `version`)
    VALUES(OLD.`username`, NULL, NULL, 1)
    ON DUPLICATE KEY UPDATE `bytes`=NULL, `messages`=NULL, `version`=`version`+1
SQL);
    }

    public function down(Schema $schema): void
    {
        $this->addSql(/** @lang MySQL */ 'DROP TABLE dovecot_quota');
        $this->addSql(/** @lang MySQL */ 'DROP TABLE dovecot_quota_changelog');
        $this->addSql(/** @lang MySQL */ 'DROP TRIGGER `dovecot_quota_insert`');
        $this->addSql(/** @lang MySQL */ 'DROP TRIGGER `dovecot_quota_update`');
        $this->addSql(/** @lang MySQL */ 'DROP TRIGGER `dovecot_quota_delete`');
    }

    public function isTransactional(): bool
    {
        return false;
    }
}
