<?php

declare(strict_types=1);

namespace App\DocumentExporter;

use App\Document\MailDkim;

final class MailDkimExporter
{
    public function exportDetail(MailDkim $dkim): array
    {
        return [
            'isEnabled' => $dkim->isEnabled(),
            'key' => '<set>',
            'selector' => $dkim->getSelector(),
        ];
    }
}
