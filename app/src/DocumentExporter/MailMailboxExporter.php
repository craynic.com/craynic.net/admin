<?php

declare(strict_types=1);

namespace App\DocumentExporter;

use App\Document\MailMailbox;
use DateTimeInterface;

final readonly class MailMailboxExporter
{
    public function __construct(
        private MailAppPasswordExporter $appPasswordExporter,
        private MailBccExporter $bccExporter,
        private MailExtraSenderExporter $extraSenderExporter,
        private MailMailboxRedirectExporter $mailboxRedirectExporter,
    ) {
    }

    public function exportDetail(MailMailbox $mailbox): array
    {
        return [
            'id' => $mailbox->getId()->toString(),
            'mailbox' => $mailbox->getName(),
            'description' => $mailbox->getDescription(),
            'password' => $mailbox->getPassword() !== null
                ? '<set>' : '<unset>',
            'isActive' => $mailbox->isActive(),
            'isLoginEnabled' => $mailbox->isLoginEnabled(),
            'isDeliveryEnabled' => $mailbox->isDeliveryEnabled(),
            'isCatchAll' => $mailbox->isCatchAll(),
            'extraSenders' => $mailbox->getExtraSenders()->map(
                    $this->extraSenderExporter->exportDetail(...),
                )->toArray(),
            'bcc' => $this->bccExporter->exportDetail($mailbox->getBcc()),
            'appPasswords' => $mailbox->getAppPasswords()->map(
                    $this->appPasswordExporter->exportDetail(...),
                )->toArray(),
            'lastLogin' => $mailbox->getLastLogin()?->format(DateTimeInterface::RFC3339_EXTENDED),
            'redirect' => $mailbox->getRedirect()
                ? $this->mailboxRedirectExporter->exportDetail($mailbox->getRedirect())
                : null,
            'junkFolderName' => $mailbox->getJunkFolderName(),
        ];
    }
}
