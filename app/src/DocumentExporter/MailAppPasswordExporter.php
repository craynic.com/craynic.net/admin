<?php

declare(strict_types=1);

namespace App\DocumentExporter;

use App\Document\MailAppPassword;
use DateTimeInterface;

final class MailAppPasswordExporter
{
    public function exportDetail(MailAppPassword $password): array
    {
        return [
            'description' => $password->getDescription(),
            'password' => '<set>',
            'lastUsed' => $password->getLastUsed()?->format(DateTimeInterface::RFC3339_EXTENDED),
        ];
    }
}
