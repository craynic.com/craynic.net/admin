<?php

declare(strict_types=1);

namespace App\DocumentExporter;

use App\Document\MailBcc;

final readonly class MailBccExporter
{
    public function exportDetail(?MailBcc $mailBcc): ?array
    {
        if ($mailBcc === null) {
            return null;
        }

        return [
            'bcc' => $mailBcc->getBcc(),
            'isActive' => $mailBcc->isActive(),
        ];
    }
}
