<?php

declare(strict_types=1);

namespace App\DocumentExporter;

use App\Document\MailAlias;

final readonly class MailAliasExporter
{
    public function exportList(MailAlias ...$aliases): array
    {
        return array_map(
            fn (MailAlias $alias): string => $alias->getDomain(),
            $aliases
        );
    }

    public function exportDetail(MailAlias $alias): array
    {
        return [
            'domain' => $alias->getDomain(),
            'isActive' => $alias->isActive(),
            'isDeliveryEnabled' => $alias->isDeliveryEnabled(),
        ];
    }
}
