<?php

declare(strict_types=1);

namespace App\DocumentExporter;

use App\Document\MailExtraSender;

final class MailExtraSenderExporter
{
    public function exportDetail(MailExtraSender $extraSender): array
    {
        return [
            'sender' => $extraSender->getSender(),
            'isActive' => $extraSender->isActive(),
        ];
    }
}
