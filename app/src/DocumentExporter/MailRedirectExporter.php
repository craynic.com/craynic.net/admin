<?php

declare(strict_types=1);

namespace App\DocumentExporter;

use App\Document\MailRedirect;

final readonly class MailRedirectExporter
{
    public function __construct(
        private MailTargetExporter $targetExporter,
    ) {
    }

    public function exportDetail(MailRedirect $redirect): array
    {
        return [
            'id' => $redirect->getId()->toString(),
            'mailbox' => $redirect->getMailbox(),
            'description' => $redirect->getDescription(),
            'targets' => $redirect->getTargets()->map(
                    $this->targetExporter->exportDetail(...)
                )->toArray(),
            'isActive' => $redirect->isActive(),
        ];
    }

    public function exportList(MailRedirect ...$redirects): array
    {
        return array_map(
            fn (MailRedirect $redirect): array => [
                'id' => $redirect->getId()->toString(),
                'mailbox' => $redirect->getMailbox(),
            ],
            $redirects
        );
    }
}
