<?php

declare(strict_types=1);

namespace App\DocumentExporter;

use App\Document\MailTarget;

final readonly class MailTargetExporter
{
    public function exportDetail(MailTarget $target): array
    {
        return [
            'targetAddress' => $target->getTargetAddress(),
            'isActive' => $target->isActive(),
        ];
    }
}
