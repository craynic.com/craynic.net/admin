<?php

declare(strict_types=1);

namespace App\ValueObject;

use InvalidArgumentException;

final readonly class QuotaUsageUpdateResult
{
    public function __construct(public int $updated, public int $notDeleted)
    {
        if ($this->updated < 0) {
            throw new InvalidArgumentException('Updated count cannot be negative.');
        }

        if ($this->notDeleted < 0) {
            throw new InvalidArgumentException('Not-deleted count cannot be negative.');
        }

        if ($this->notDeleted > $this->updated) {
            throw new InvalidArgumentException('Not-deleted count cannot be bigger than updated count.');
        }
    }
}
