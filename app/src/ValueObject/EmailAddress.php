<?php

declare(strict_types=1);

namespace App\ValueObject;

use App\Exception\InvalidEmailAddress;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\EmailValidator;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\ConstraintValidatorFactory;
use Symfony\Component\Validator\Validation;

final readonly class EmailAddress
{
    public string $domain;
    public string $mailbox;

    public function __construct(string $emailAddress)
    {
        $lcEmailAddress = strtolower($emailAddress);

        $this->validate($lcEmailAddress);

        $this->domain = preg_replace('/^.*@/', '', $lcEmailAddress);
        $this->mailbox = substr($lcEmailAddress, 0, -strlen($this->domain) - 1);
    }

    public static function fromValues(string $domain, string $mailbox): EmailAddress
    {
        return new EmailAddress(sprintf('%s@%s', $mailbox, $domain));
    }

    private function validate(string $emailAddress): void
    {
        $builder = Validation::createValidatorBuilder()
            ->setConstraintValidatorFactory(
                new ConstraintValidatorFactory([
                    EmailValidator::class => new EmailValidator(Email::VALIDATION_MODE_HTML5)
                ])
            );

        $validator = $builder->getValidator();
        $violations = $validator->validate($emailAddress, [
            new Email(mode: Email::VALIDATION_MODE_STRICT),
            new NotBlank(),
        ]);

        if (count($violations) !== 0) {
            throw new InvalidEmailAddress($emailAddress);
        }
    }

    public function __toString(): string
    {
        return sprintf('%s@%s', $this->mailbox, $this->domain);
    }
}
