<?php

declare(strict_types=1);

namespace App;

use App\Service\ReadModel\ReadModel;
use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Component\HttpKernel\Kernel as SymfonyKernel;

class Kernel extends SymfonyKernel
{
    use MicroKernelTrait;

    public function getProjectDir(): string
    {
        return __DIR__ . '/..';
    }

    public function getCacheDir(): string
    {
        return sys_get_temp_dir() . '/cache/';
    }

    public function getLogDir(): string
    {
        return sys_get_temp_dir() . '/log/';
    }

    public function build(ContainerBuilder $container): void
    {
        parent::build($container);

        $container
            ->registerForAutoconfiguration(ReadModel::class)
            ->addTag('app.readmodel');
    }

    /** @noinspection PhpUnusedPrivateMethodInspection */
    private function configureContainer(ContainerConfigurator $container): void
    {
        $configDir = $this->getConfigDir();

        $container->import($configDir . '/{packages}/*.{yaml,php}');
        $container->import($configDir . '/{packages}/' . $this->environment . '/*.{yaml,php}');

        $container->import($configDir . '/services.{yaml,php}');
        $container->import($configDir . '/services_' . $this->environment . '.{yaml,php}');
    }
}
