<?php

declare(strict_types=1);

namespace App\Exception;

use App\ValueObject\EmailAddress;
use RuntimeException;

final class QuotaMailboxNotFound extends RuntimeException
{
    public function __construct(public readonly EmailAddress $mailboxEmailAddress)
    {
        parent::__construct();
    }
}
