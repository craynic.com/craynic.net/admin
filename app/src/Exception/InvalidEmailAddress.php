<?php

declare(strict_types=1);

namespace App\Exception;

use InvalidArgumentException;

class InvalidEmailAddress extends InvalidArgumentException
{
    public function __construct(public readonly string $emailAddress)
    {
        parent::__construct(sprintf('String "%s" is not a valid e-mail address.', $emailAddress));
    }
}
