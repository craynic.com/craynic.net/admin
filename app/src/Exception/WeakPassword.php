<?php

declare(strict_types=1);

namespace App\Exception;

use RuntimeException;

class WeakPassword extends RuntimeException
{
    public function __construct()
    {
        parent::__construct('The provided password is weak.');
    }
}
