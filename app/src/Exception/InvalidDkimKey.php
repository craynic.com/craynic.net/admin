<?php

namespace App\Exception;

use RuntimeException;

final class InvalidDkimKey extends RuntimeException
{
    public function __construct()
    {
        parent::__construct();
    }
}
