<?php

declare(strict_types=1);

namespace App\Exception;

use RuntimeException;

final class DkimNotConfigured extends RuntimeException
{
    public function __construct(public readonly string $domainName)
    {
        parent::__construct();
    }
}
