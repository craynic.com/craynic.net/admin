<?php

declare(strict_types=1);

namespace App\Exception;

use RuntimeException;

final class MailboxNotFound extends RuntimeException
{
    public function __construct(public readonly string $mailboxIdentifier)
    {
        parent::__construct();
    }
}
