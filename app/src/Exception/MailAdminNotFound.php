<?php

declare(strict_types=1);

namespace App\Exception;

use RuntimeException;

final class MailAdminNotFound extends RuntimeException
{
    public function __construct(public readonly string $emailAddress)
    {
        parent::__construct();
    }
}
