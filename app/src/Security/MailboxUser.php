<?php

namespace App\Security;

use App\Document\MailAdmin;
use App\Document\MailDomain;
use App\Document\MailMailbox;
use App\Exception\TwoFactorGoogleAuthNotSet;
use App\Exception\TwoFactorNotSet;
use League\OAuth2\Server\Entities\UserEntityInterface;
use Scheb\TwoFactorBundle\Model\Google\TwoFactorInterface;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

final readonly class MailboxUser implements
    UserInterface,
    PasswordAuthenticatedUserInterface,
    UserEntityInterface,
    OAuth2UserInterface,
    TwoFactorInterface
{
    public const string ROLE_MAILBOX_USER = 'ROLE_MAILBOX_USER';

    public function __construct(
        private string $userIdentifier,
        private ?string $password,
        private bool $isActive,
        private bool $isMailboxAdmin,
        private bool $isGoogleAuthenticatorEnabled,
        private ?string $googleAuthenticatorSecret,
    ) {
    }

    public static function fromDocuments(MailDomain $domain, MailMailbox $mailbox, ?MailAdmin $admin): self
    {
        try {
            $twoFactorAuth = $mailbox->getTwoFactorAuth();
            $googleAuth = $twoFactorAuth->getGoogleAuth();
            $isGoogleAuthenticatorEnabled = $googleAuth->isEnabled();
            $googleSecret = $googleAuth->getSecret();
        } catch (TwoFactorNotSet | TwoFactorGoogleAuthNotSet) {
            $isGoogleAuthenticatorEnabled = false;
            $googleSecret = null;
        }

        return new self(
            userIdentifier: sprintf('%s@%s', $mailbox->getName(), $domain->getName()),
            password: $mailbox->getPassword(),
            isActive: $mailbox->isActive()
                && $domain->isActive()
                && $mailbox->isLoginEnabled()
                && $domain->isLoginEnabled(),
            isMailboxAdmin: $admin !== null,
            isGoogleAuthenticatorEnabled: $isGoogleAuthenticatorEnabled,
            googleAuthenticatorSecret: $googleSecret,
        );
    }

    public function getRoles(): array
    {
        return [self::ROLE_MAILBOX_USER];
    }

    public function eraseCredentials(): void
    {
    }

    public function getUserIdentifier(): string
    {
        return $this->userIdentifier;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function getIdentifier(): string
    {
        return $this->getUserIdentifier();
    }

    public function isActive(): bool
    {
        return $this->isActive;
    }

    public function isGoogleAuthenticatorEnabled(): bool
    {
        return $this->isGoogleAuthenticatorEnabled;
    }

    public function getGoogleAuthenticatorUsername(): string
    {
        return $this->getUserIdentifier();
    }

    public function getGoogleAuthenticatorSecret(): ?string
    {
        return $this->googleAuthenticatorSecret;
    }

    public function isMailboxAdmin(): bool
    {
        return $this->isMailboxAdmin;
    }

    public function __serialize(): array
    {
        return [
            'userIdentifier' => $this->getUserIdentifier(),
            'password' => $this->getPassword(),
        ];
    }
}
