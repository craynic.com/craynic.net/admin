<?php

namespace App\Security;

use App\Exception\InvalidEmailAddress;
use App\Exception\MailAdminNotFound;
use App\Exception\MailboxNotFound;
use App\Exception\MailDomainNotFound;
use App\Repository\MailAdminRepository;
use App\Repository\MailDomainRepository;
use App\ValueObject\EmailAddress;
use Symfony\Component\Security\Core\Exception\UserNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

final readonly class MailboxUserProvider implements UserProviderInterface
{
    public function __construct(
        private MailDomainRepository $mailDomainRepository,
        private MailAdminRepository $mailAdminRepository
    ) {
    }

    public function refreshUser(UserInterface $user): UserInterface
    {
        return $this->loadUserByIdentifier($user->getUserIdentifier());
    }

    public function supportsClass(string $class): bool
    {
        return $class === MailboxUser::class;
    }

    public function loadUserByIdentifier(string $identifier): MailboxUser
    {
        try {
            $emailAddress = new EmailAddress($identifier);
            $domain = $this->mailDomainRepository->findOneByDomainName($emailAddress->domain);
            $mailbox = $domain->getMailboxByName($emailAddress->mailbox);
        } catch (InvalidEmailAddress | MailDomainNotFound | MailboxNotFound) {
            throw new UserNotFoundException();
        }

        try {
            $admin = $this->mailAdminRepository->getAdminByEmail($emailAddress);
        } catch (MailAdminNotFound) {
            $admin = null;
        }

        $user = MailboxUser::fromDocuments(domain: $domain, mailbox: $mailbox, admin: $admin);

        if (!$user->isActive()) {
            throw new UserNotFoundException();
        }

        return $user;
    }
}
