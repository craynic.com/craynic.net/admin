<?php

namespace App\Security;

use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;

final readonly class MailboxAppUser implements PasswordAuthenticatedUserInterface
{
    public function __construct(private string $appPassword)
    {
    }

    public function getPassword(): string
    {
        return $this->appPassword;
    }
}
