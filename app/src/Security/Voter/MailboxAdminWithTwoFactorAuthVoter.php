<?php

/** @noinspection PhpUnused */

declare(strict_types=1);

namespace App\Security\Voter;

use App\Security\MailboxUser;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

final class MailboxAdminWithTwoFactorAuthVoter extends Voter
{
    public const string CAN_SWITCH_TO_ANOTHER_MAILBOX_DISABLED = 'CAN_SWITCH_TO_ANOTHER_MAILBOX_DISABLED';
    public const string CAN_SWITCH_TO_ANOTHER_MAILBOX = 'CAN_SWITCH_TO_ANOTHER_MAILBOX';

    protected function supports(string $attribute, mixed $subject): bool
    {
        return in_array(
            $attribute,
            [self::CAN_SWITCH_TO_ANOTHER_MAILBOX_DISABLED, self::CAN_SWITCH_TO_ANOTHER_MAILBOX],
            true
        );
    }

    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        if (!$user instanceof MailboxUser || !$user->isMailboxAdmin()) {
            return false;
        }

        return match ($attribute) {
            self::CAN_SWITCH_TO_ANOTHER_MAILBOX_DISABLED => !$user->isGoogleAuthenticatorEnabled(),
            self::CAN_SWITCH_TO_ANOTHER_MAILBOX => $user->isGoogleAuthenticatorEnabled(),
        };
    }
}
