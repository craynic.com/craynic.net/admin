<?php

declare(strict_types=1);

namespace App\Controller;

use App\Security\MailboxUser;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\Voter\AuthenticatedVoter;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Http\Attribute\CurrentUser;
use Symfony\Component\Security\Http\Attribute\IsGranted;

final class WebmailController extends AbstractController
{
    #[Route('/webmail/logout', methods: [Request::METHOD_GET])]
    public function logoutWebhookAction(
        Request $request,
        TokenStorageInterface $tokenStorage,
        #[CurrentUser] ?UserInterface $user
    ): RedirectResponse {
        $webmailUrl = $this->getParameter('app.webmail.url');

        if (empty($webmailUrl)) {
            throw $this->createNotFoundException();
        }

        if ($user instanceof MailboxUser) {
            $request->getSession()->invalidate();
            $tokenStorage->setToken();

            return $this->redirect($webmailUrl);
        }

        if ($user === null) {
            return $this->redirect($webmailUrl);
        }

        return $this->redirect(
            $this->generateUrl('webmail-logout', ['_locale' => $request->getLocale()])
        );
    }

    #[Route(
        '/{_locale}/webmail/logout',
        name: 'webmail-logout',
        requirements: ['_locale' => '%app.locale.route_requirements%']
    )]
    #[IsGranted(AuthenticatedVoter::IS_AUTHENTICATED)]
    public function logoutAction(): Response
    {
        $webmailUrl = $this->getParameter('app.webmail.url');

        return $this->render('webmail/logout.html.twig', [
            'username' => $this->getUser()->getUserIdentifier(),
            'webmailUrl' => $webmailUrl,
        ]);
    }
}
