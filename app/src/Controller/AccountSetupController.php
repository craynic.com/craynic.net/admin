<?php

declare(strict_types=1);

namespace App\Controller;

use App\Exception\TwoFactorBackupCodesAlreadyExist;
use App\Exception\TwoFactorBackupCodesNotSet;
use App\Exception\TwoFactorGoogleAuthNotSet;
use App\Exception\TwoFactorNotSet;
use App\Repository\MailMailboxRepository;
use App\Security\MailboxUser;
use App\Service\TwoFactorAuth\BackupCodesGenerator;
use Doctrine\ODM\MongoDB\DocumentManager;
use Endroid\QrCode\Builder\Builder;
use Endroid\QrCode\Encoding\Encoding;
use Endroid\QrCode\ErrorCorrectionLevel;
use Endroid\QrCode\RoundBlockSizeMode;
use Endroid\QrCode\Writer\PngWriter;
use Scheb\TwoFactorBundle\Security\TwoFactor\Provider\Google\GoogleAuthenticatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authorization\Voter\AuthenticatedVoter;
use Symfony\Component\Security\Http\Attribute\CurrentUser;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/{_locale}/account/setup', requirements: ['_locale' => '%app.locale.route_requirements%'])]
#[IsGranted(MailboxUser::ROLE_MAILBOX_USER)]
#[IsGranted(AuthenticatedVoter::IS_AUTHENTICATED_FULLY)]
final class AccountSetupController extends AbstractController
{
    private const int GOOGLE_AUTH_QR_CODE_SIZE = 300;

    public function __construct(
        private readonly GoogleAuthenticatorInterface $googleAuthenticator,
        private readonly DocumentManager $documentManager,
        private readonly TranslatorInterface $translator,
        private readonly BackupCodesGenerator $backupCodesGenerator,
        private readonly MailMailboxRepository $mailboxRepository,
    ) {
    }

    #[Route('', name: 'setup-account', methods: [Request::METHOD_GET, Request::METHOD_HEAD])]
    public function setupAction(#[CurrentUser] MailboxUser $user): Response
    {
        try {
            $backupCodesCount = count(
                $this->mailboxRepository->findByMailboxUser($user)->getTwoFactorAuth()->getBackupCodes()
            );
        } catch (TwoFactorNotSet | TwoFactorGoogleAuthNotSet) {
            $backupCodesCount = 0;
        }

        return $this->render('account/setup/index.html.twig', [
            'is2FAActive' => $user->isGoogleAuthenticatorEnabled(),
            'backupCodesCount' => $backupCodesCount,
        ]);
    }

    #[Route('/toggle-2fa', name: 'toggle-2fa', methods: [Request::METHOD_POST])]
    public function toggleAction(Request $request, #[CurrentUser] MailboxUser $user): Response
    {
        $mailbox = $this->mailboxRepository->findByMailboxUser($user);
        $enable = $request->request->get('2fa_state');

        if ($enable) {
            $mailbox->recreateTwoFactorAuth()->createGoogleAuth(
                secret: $this->googleAuthenticator->generateSecret()
            );
        } else {
            $mailbox->removeTwoFactorAuth();
        }

        $this->documentManager->flush();

        return $this->redirectToRoute($enable ? 'verify-2fa' : 'setup-account');
    }

    #[Route('/verify-2fa', name: 'verify-2fa', methods: [Request::METHOD_GET])]
    public function verifyAction(#[CurrentUser] MailboxUser $user): Response
    {
        if ($user->isGoogleAuthenticatorEnabled()) {
            return $this->redirectToRoute('setup-account');
        }

        $qrCode = Builder::create()
            ->writer(new PngWriter())
            ->writerOptions([])
            ->data($this->googleAuthenticator->getQRContent($user))
            ->encoding(new Encoding('UTF-8'))
            ->errorCorrectionLevel(ErrorCorrectionLevel::High)
            ->size(self::GOOGLE_AUTH_QR_CODE_SIZE)
            ->margin(0)
            ->roundBlockSizeMode(RoundBlockSizeMode::Shrink)
            ->build()
            ->getDataUri();

        $googleSecret = $user->getGoogleAuthenticatorSecret();
        $googleSecretFormatted = implode(' ', str_split($googleSecret, 4));

        return $this->render('account/setup/verify-2fa.html.twig', [
            'googleSecret' => $user->getGoogleAuthenticatorSecret(),
            'googleSecretFormatted' => $googleSecretFormatted,
            'qrCodeDataURI' => $qrCode,
        ]);
    }

    #[Route('/verify-2fa', name: 'verify-2fa-post', methods: [Request::METHOD_POST])]
    public function postVerifyAction(Request $request, #[CurrentUser] MailboxUser $user): Response
    {
        if ($user->isGoogleAuthenticatorEnabled()) {
            return $this->redirectToRoute('setup-account');
        }

        $authCode = (string) $request->request->get('auth_code');

        if (!$this->googleAuthenticator->checkCode($user, $authCode)) {
            $this->addFlash('error', $this->translator->trans('Invalid authentication code'));

            return $this->redirectToRoute('verify-2fa');
        }

        $mailbox = $this->mailboxRepository->findByMailboxUser($user);

        $mailbox->getTwoFactorAuth()->getGoogleAuth()->enable();

        $this->documentManager->flush();

        $this->addFlash('success', $this->translator->trans('The two-factor verification was enabled.'));

        return $this->redirectToRoute(
            'backup-codes-2fa-generate',
            ['request' => $request],
            Response::HTTP_TEMPORARY_REDIRECT
        );
    }

    #[Route('/backup-codes-2fa', name: 'backup-codes-2fa', methods: [Request::METHOD_GET])]
    public function backupCodesAction(#[CurrentUser] MailboxUser $user): Response
    {
        if (!$user->isGoogleAuthenticatorEnabled()) {
            throw $this->createAccessDeniedException();
        }

        try {
            $backupCodesCount = count(
                $this->mailboxRepository->findByMailboxUser($user)->getTwoFactorAuth()->getBackupCodes()
            );
        } catch (TwoFactorNotSet | TwoFactorGoogleAuthNotSet) {
            $backupCodesCount = 0;
        }

        return $this->render('account/setup/backup-codes-2fa.html.twig', [
            'backupCodesCount' => $backupCodesCount,
        ]);
    }

    #[Route('/backup-codes-2fa', name: 'backup-codes-2fa-generate', methods: [Request::METHOD_POST])]
    public function backupCodesGenerateAction(#[CurrentUser] MailboxUser $user): Response
    {
        if (!$user->isGoogleAuthenticatorEnabled()) {
            throw $this->createAccessDeniedException();
        }

        try {
            $rawBackupCodes = $this->backupCodesGenerator->generateForUser($user);

            $this->addFlash('success', $this->translator->trans('New backup codes were generated.'));
        } catch (TwoFactorNotSet) {
            throw $this->createAccessDeniedException();
        } catch (TwoFactorBackupCodesAlreadyExist) {
            return $this->redirectToRoute('backup-codes-2fa');
        }

        $this->documentManager->flush();

        return $this->render('account/setup/backup-codes-2fa-generated.html.twig', [
            'backupCodes' => $rawBackupCodes,
        ]);
    }

    #[Route('/backup-codes-2fa-regenerate', name: 'backup-codes-2fa-regenerate', methods: [Request::METHOD_POST])]
    public function backupCodesRegenerateAction(Request $request, #[CurrentUser] MailboxUser $user): Response
    {
        if (!$user->isGoogleAuthenticatorEnabled()) {
            throw $this->createAccessDeniedException();
        }

        try {
            $mailbox = $this->mailboxRepository->findByMailboxUser($user);

            $mailbox->getTwoFactorAuth()->removeBackupCodes();

            $this->documentManager->flush();
        } catch (TwoFactorBackupCodesNotSet) {
        } catch (TwoFactorNotSet) {
            throw $this->createAccessDeniedException();
        }

        return $this->redirectToRoute(
            'backup-codes-2fa-generate',
            ['request' => $request],
            Response::HTTP_TEMPORARY_REDIRECT
        );
    }
}
