<?php

declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/autodiscover')]
final class AutodiscoverController extends AbstractController
{
    #[Route('/autodiscover.xml', defaults: ['_format' => 'xml'], methods: [Request::METHOD_POST])]
    public function accountAction(): Response
    {
        // TODO: implement autodiscover protocol
        throw $this->createNotFoundException();
    }
}
