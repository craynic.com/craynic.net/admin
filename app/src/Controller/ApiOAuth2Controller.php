<?php

declare(strict_types=1);

namespace App\Controller;

use App\Exception\AccessTokenNotFound;
use App\Service\OAuth2IntrospectionService;
use App\Service\OAuth2UserInfoService;
use League\Bundle\OAuth2ServerBundle\Security\Authentication\Token\OAuth2Token;
use League\OAuth2\Server\Exception\OAuthServerException;
use LogicException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/api/oauth2', format: 'json')]
final class ApiOAuth2Controller extends AbstractController
{
    #[Route('/token', name: 'api-oauth2-token', methods: [Request::METHOD_POST])]
    public function tokenAction(Request $request): Response
    {
        return $this->forward(
            controller: 'league.oauth2_server.controller.token::indexAction',
            query: $request->query->all()
        );
    }

    #[Route('/authorize', name: 'api-oauth2-authorize', methods: [Request::METHOD_GET, Request::METHOD_HEAD])]
    public function authorizeAction(Request $request): Response
    {
        return $this->forward(
            controller: 'league.oauth2_server.controller.authorization::indexAction',
            query: $request->query->all()
        );
    }

    #[Route(
        '/introspection',
        name: 'api-oauth2-introspection',
        methods: [Request::METHOD_GET, Request::METHOD_HEAD]
    )]
    #[IsGranted('ROLE_OAUTH2_EMAIL')]
    public function introspectionAction(
        Security $security,
        OAuth2IntrospectionService $introspectionService
    ): JsonResponse {
        $token = $security->getToken();
        if (!$token instanceof OAuth2Token) {
            throw new LogicException('Invalid token type for OAuth2 authentication');
        }

        try {
            return new JsonResponse($introspectionService->getIntrospectionDataFromToken($token));
        } catch (AccessTokenNotFound $exception) {
            throw $this->createNotFoundException(previous: $exception);
        }
    }

    #[Route('/userinfo', name: 'api-oauth2-userinfo', methods: [Request::METHOD_GET, Request::METHOD_HEAD])]
    public function userinfoAction(
        Security $security,
        Request $request,
        OAuth2UserInfoService $userInfoService
    ): JsonResponse {
        try {
            if ($request->query->has('access_token')) {
                return new JsonResponse(
                    $userInfoService->getUserInfoDataFromAccessToken($request->query->get('access_token'))
                );
            }

            $token = $security->getToken();
            if ($token instanceof OAuth2Token) {
                return new JsonResponse($userInfoService->getUserInfoDataFromOAuth2Token($token));
            }
        } catch (OAuthServerException $exception) {
            throw $this->createAccessDeniedException(previous: $exception);
        }

        throw $this->createAccessDeniedException();
    }
}
