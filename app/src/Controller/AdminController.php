<?php

declare(strict_types=1);

namespace App\Controller;

use App\Exception\InvalidEmailAddress;
use App\Repository\MailAdminRepository;
use App\Security\MailboxUser;
use App\Security\Voter\MailboxAdminWithTwoFactorAuthVoter;
use App\Service\ManagedAccountService;
use App\ValueObject\EmailAddress;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authorization\Voter\AuthenticatedVoter;
use Symfony\Component\Security\Http\Attribute\CurrentUser;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/{_locale}/admin', requirements: ['_locale' => '%app.locale.route_requirements%'])]
#[IsGranted(MailboxUser::ROLE_MAILBOX_USER)]
final class AdminController extends AbstractController
{
    public function __construct(
        private readonly TranslatorInterface $translator,
        private readonly ManagedAccountService $managedAccountService,
        private readonly MailAdminRepository $mailAdminRepository,
    ) {
    }

    #[Route(
        '/switch-to-managed-account',
        name: 'admin-switch-to-managed-account',
        methods: [Request::METHOD_GET, Request::METHOD_HEAD, Request::METHOD_POST],
    )]
    #[IsGranted(MailboxAdminWithTwoFactorAuthVoter::CAN_SWITCH_TO_ANOTHER_MAILBOX)]
    #[IsGranted(AuthenticatedVoter::IS_AUTHENTICATED_FULLY)]
    public function chooseManagedAccountAction(Request $request, #[CurrentUser] MailboxUser $user): Response
    {
        if ($request->getMethod() === Request::METHOD_POST) {
            try {
                $mailboxToAccess = new EmailAddress((string)$request->request->get('mailbox'));
            } catch (InvalidEmailAddress) {
                $this->addFlash('error', $this->translator->trans(
                    'Invalid e-mail address.'
                ));

                return $this->redirect($request->getRequestUri());
            }

            if (!$user->isMailboxAdmin()) {
                throw $this->createAccessDeniedException();
            }

            $mailAdmin = $this->mailAdminRepository->findByMailboxUser($user);

            if (!$this->managedAccountService->isManagedAccount($mailAdmin, $mailboxToAccess)) {
                $this->addFlash('error', $this->translator->trans(
                    'Given mailbox does not exist or is not managed by the logged-in user.'
                ));

                return $this->redirect($request->getRequestUri());
            }

            $targetPath = (string) $request->get('_target_path');
            $targetPath .= sprintf(
                '%s%s=%s',
                str_contains($targetPath, '?') ? '&' : '?',
                '_mailbox_to_access',
                $mailboxToAccess
            );

            return $this->redirect($targetPath);
        }

        return $this->render('admin/choose-managed-account.html.twig');
    }
}
