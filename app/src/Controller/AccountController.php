<?php

declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\ServiceUnavailableHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authorization\Voter\AuthenticatedVoter;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/{_locale}/account', requirements: ['_locale' => '%app.locale.route_requirements%'])]
final class AccountController extends AbstractController
{
    public function __construct(private readonly TranslatorInterface $translator)
    {
    }

    #[Route('', name: 'account', methods: [Request::METHOD_GET, Request::METHOD_HEAD])]
    #[IsGranted(AuthenticatedVoter::IS_AUTHENTICATED)]
    public function accountAction(): Response
    {
        return $this->render('account/index.html.twig');
    }

    #[Route(
        '/login',
        name: 'account-login-form',
        methods: [Request::METHOD_GET, Request::METHOD_POST, Request::METHOD_HEAD]
    )]
    public function loginFormAction(AuthenticationUtils $authenticationUtils): Response
    {
        $lastAuthError = $authenticationUtils->getLastAuthenticationError();
        if ($lastAuthError !== null) {
            $this->addFlash('error', $this->translator->trans(
                $lastAuthError->getMessageKey(),
                $lastAuthError->getMessageData(),
                'security')
            );
        }

        return $this->render('account/login-form.html.twig', [
            'last_username' => $authenticationUtils->getLastUsername(),
        ]);
    }

    #[Route('/logout', name: 'account-logout', methods: [Request::METHOD_GET, Request::METHOD_HEAD])]
    public function logoutAction(): Response
    {
        // this route is handled by Symfony security's form_login handler - this exception should never occur
        throw new ServiceUnavailableHttpException();
    }
}
