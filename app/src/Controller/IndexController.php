<?php

declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/')]
final class IndexController extends AbstractController
{
    #[Route('', name: 'index', methods: [Request::METHOD_GET, Request::METHOD_HEAD])]
    public function indexAction(Request $request): Response
    {
        return $this->redirectToRoute('account', ['_locale' => $request->getLocale()]);
    }
}
