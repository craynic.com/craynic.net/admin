<?php

declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api', format: 'json')]
final class ApiController extends AbstractController
{
    #[Route('', name: 'api-index', methods: [Request::METHOD_GET, Request::METHOD_HEAD])]
    public function indexAction(): Response
    {
        return new JsonResponse(['status' => 'ok']);
    }
}
