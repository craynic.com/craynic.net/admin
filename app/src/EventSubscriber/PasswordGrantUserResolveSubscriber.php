<?php

declare(strict_types=1);

namespace App\EventSubscriber;

use App\Repository\MailMailboxRepository;
use App\Security\MailboxAppUser;
use App\Security\MailboxUserProvider;
use App\Service\MailboxLastLoginUpdater;
use JetBrains\PhpStorm\ArrayShape;
use League\Bundle\OAuth2ServerBundle\Event\UserResolveEvent;
use League\Bundle\OAuth2ServerBundle\OAuth2Events;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\Exception\UserNotFoundException;

final readonly class PasswordGrantUserResolveSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private MailboxUserProvider $mailboxUserProvider,
        private UserPasswordHasherInterface $passwordHasher,
        private MailboxLastLoginUpdater $mailboxLastLoginUpdater,
        private MailMailboxRepository $mailboxRepository,
    ) {
    }

    #[ArrayShape([OAuth2Events::USER_RESOLVE => "string"])]
    public static function getSubscribedEvents(): array
    {
        return [
            OAuth2Events::USER_RESOLVE => 'resolve'
        ];
    }

    public function resolve(UserResolveEvent $event): void
    {
        try {
            $user = $this->mailboxUserProvider->loadUserByIdentifier($event->getUsername());
            $mailbox = $this->mailboxRepository->findByMailboxUser($user);

            foreach ($mailbox->getAppPasswords() as $appPassword) {
                $appUser = new MailboxAppUser($appPassword->getPassword());

                if ($this->passwordHasher->isPasswordValid($appUser, $event->getPassword())) {
                    $this->mailboxLastLoginUpdater->updateAppPasswordLastUsed($appPassword);
                    $event->setUser($user);
                    return;
                }
            }
        } catch (UserNotFoundException) {
        }

        $event->setUser(null);
    }
}
