<?php

declare(strict_types=1);

namespace App\EventSubscriber;

use App\Exception\MailAdminNotFound;
use App\Repository\MailAdminRepository;
use App\Security\MailboxUser;
use App\Service\ManagedAccountService;
use App\ValueObject\EmailAddress;
use JetBrains\PhpStorm\ArrayShape;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\Authentication\Token\SwitchUserToken;
use Symfony\Component\Security\Core\Authorization\Voter\AuthenticatedVoter;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\Event\SwitchUserEvent;
use Symfony\Component\Security\Http\SecurityEvents;

final readonly class SwitchUserSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private Security $security,
        private ManagedAccountService $managedAccountService,
        private MailAdminRepository $mailAdminRepository,
    ) {
    }

    #[ArrayShape([SecurityEvents::SWITCH_USER => "string"])]
    public static function getSubscribedEvents(): array
    {
        return [
            SecurityEvents::SWITCH_USER => 'onSwitchUser',
        ];
    }

    public function onSwitchUser(SwitchUserEvent $event): void
    {
        $user = $this->security->getUser();

        if (!$user instanceof MailboxUser) {
            return;
        }

        // we are not impersonating a user - probably we're exiting the impersonation now
        if (!$event->getToken() instanceof SwitchUserToken) {
            return;
        }

        $emailAddress = new EmailAddress($event->getTargetUser()->getUserIdentifier());

        if (!$this->security->isGranted(AuthenticatedVoter::IS_AUTHENTICATED_FULLY)) {
            throw new AuthenticationException();
        }

        if (!$user->isMailboxAdmin()) {
            throw new AuthenticationException();
        }

        try {
            $mailAdmin = $this->mailAdminRepository->findByMailboxUser($user);

            if (!$this->managedAccountService->isManagedAccount($mailAdmin, $emailAddress)) {
                throw new AuthenticationException();
            }
        } catch (MailAdminNotFound) {
            throw new AuthenticationException();
        }
    }
}
