<?php

declare(strict_types=1);

namespace App\EventSubscriber;

use JetBrains\PhpStorm\ArrayShape;
use League\Bundle\OAuth2ServerBundle\Event\AuthorizationRequestResolveEvent;
use League\Bundle\OAuth2ServerBundle\OAuth2Events;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

final class AuthorizationRequestResolveSubscriber implements EventSubscriberInterface
{
    public const string SESSION_AUTHORIZATION_RESULT = '_app.oauth2.authorization_result';

    public function __construct(
        private readonly RequestStack $requestStack,
        private readonly UrlGeneratorInterface $router,
    ) {
    }

    #[ArrayShape([OAuth2Events::AUTHORIZATION_REQUEST_RESOLVE => "string"])]
    public static function getSubscribedEvents(): array
    {
        return [
            OAuth2Events::AUTHORIZATION_REQUEST_RESOLVE => 'resolve',
        ];
    }

    public function resolve(AuthorizationRequestResolveEvent $event): void
    {
        $request = $this->requestStack->getCurrentRequest();

        if ($request->getSession()->has(self::SESSION_AUTHORIZATION_RESULT)) {
            $event->resolveAuthorization($request->getSession()->get(self::SESSION_AUTHORIZATION_RESULT));
            $request->getSession()->remove(self::SESSION_AUTHORIZATION_RESULT);

            return;
        }

        $event->setResponse(
            new RedirectResponse(
                $this->router->generate('oauth2-consent-form', $request->query->all())
            )
        );
    }
}
