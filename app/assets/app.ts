import './styles/app.scss';

document.querySelectorAll('[data-copy]').forEach((element: Element): void => {
    element.addEventListener('click', async (): Promise<void> => {
        await navigator.clipboard.writeText(element.getAttribute('data-copy'));

        element.classList.remove('clicked');

        window.setTimeout(
            (): void => {
                element.classList.add('clicked');
            },
            1
        );
    });
});
