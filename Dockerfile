FROM node:22@sha256:f6b9c31ace05502dd98ef777aaa20464362435dcc5e312b0e213121dcf7d8b95 AS builder

WORKDIR /tmp/build

COPY app/assets/ assets/
COPY app/package.json app/webpack.config.js app/yarn.lock app/tsconfig.json ./

RUN yarn && yarn build

FROM registry.gitlab.com/craynic.com/docker/lap:13.3.4@sha256:1178c420b50bc02babf473c6fda071ab55051c96f8ca517f20748df6fc67651a

ENV APP_ENV="" \
    APP_SECRET="" \
    APP_OAUTH_ENCRYPTION_DEFUSE_KEY="" \
    APP_DB_DSN="" \
    APP_DB_DSN_READMODELS="" \
    APP_MONGO_CONN_URI="" \
    APP_MONGO_DB="" \
    APP_MONGO_APPNAME="adminWeb" \
    APP_WEBMAIL_URL="" \
    APP_PROXY_TRUSTED_PROXIES="" \
    APP_PROXY_TRUSTED_HEADERS="" \
    APP_MAILER_DSN="" \
    APP_LOG_LEVEL="info" \
    APP_QUOTA_REPORTING_SENDER="" \
    APP_2FA_TRUSTED_DEVICE_ENCRYPTION_KEY="" \
    APP_2FA_GOOGLE_SERVER_NAME="" \
    APP_2FA_GOOGLE_ISSUER=""

WORKDIR /var/www/htdocs

COPY app/bin/ bin/
COPY app/config/ config/
COPY app/public/ public/
COPY app/src/ src/
COPY app/translations/ translations/
COPY app/.env.dist app/.htaccess app/composer.* ./
COPY files/ /
COPY --from=builder /tmp/build/public/asset/ public/asset/

RUN composer install --no-dev \
    && chown -R www-data:www-data . \
    && chmod -R g+ws,o-rwx .
