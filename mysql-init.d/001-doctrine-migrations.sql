-- Since MySQL does not allow to first insert data and then define the schema
-- (as opposed to MongoDB), and because we need the OAuth2 tables to already
-- contain data, we need to create the table structure for OAuth2 tables already
-- in the init phase. And because that would collide with the existing migration
-- (migration to create the OAuth2 tables in production), we need to mark
-- the migration as finished here. Which means we have to create the table
-- for MySQL migrations here too.

CREATE TABLE `doctrine_migration_versions` (
    `version` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
    `executed_at` datetime DEFAULT NULL,
    `execution_time` int DEFAULT NULL,
    PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

INSERT INTO `doctrine_migration_versions` (`version`, `executed_at`, `execution_time`) VALUES
    ('App\\Migrations\\MySQL\\Main\\Version20220320125022', '2022-03-20 14:07:09', 91);
