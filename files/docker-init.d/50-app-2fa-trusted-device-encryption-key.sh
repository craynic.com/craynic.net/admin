#!/usr/bin/env bash

set -Eeuo pipefail

APP_2FA_TRUSTED_DEVICE_ENCRYPTION_KEY="${APP_2FA_TRUSTED_DEVICE_ENCRYPTION_KEY:-""}"

if [[ -z "$APP_2FA_TRUSTED_DEVICE_ENCRYPTION_KEY" ]]; then
  echo "Plese define APP_2FA_TRUSTED_DEVICE_ENCRYPTION_KEY" >/dev/stderr
  exit 1
fi

exit 0
